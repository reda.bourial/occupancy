django_console:
	docker exec -it occupancy_django_1 /bin/bash

react_console:
	docker exec -it occupancy_frontend_1 bash
	

go_console:
	docker exec -it occupancy_websocket_1 bash
	
docker_up:
	docker-compose build
	docker-compose up -d

docker_delete:
	docker rm  -f $$(docker ps -a -q --filter="name=occupancy")

docker_delete_images:
	docker rmi $$(docker images  | grep occupancy  | cut -d" " -f1)

docker_restart:
	docker restart   $$(docker ps -a -q --filter="name=occupancy")

restart: docker_delete
	sudo rm -rf mariadb redis
	mkdir mariadb redis
	make docker_up
	echo waiting for db to show up 
	sleep 25
	docker exec occupancy_django_1 python manage.py makemigrations
	docker exec occupancy_django_1 python manage.py migrate
	docker restart occupancy_worker_1 occupancy_daemon_1

mock:
	docker exec occupancy_django_1 python manage.py run_sensors 100

docker_prod_up:
	docker-compose -f docker-compose.prod.yml up -d

lint:
	pylint --rcfile=.pylintrc sensor/
