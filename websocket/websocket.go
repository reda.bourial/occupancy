package main

import (
	"encoding/json"
	"net/http"
	"strconv"
	"sync"

	"github.com/gorilla/websocket"
	"gitlab.com/reda.bourial/catch"
	"gitlab.com/reda.bourial/log4g"
)

var upgrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}
var upgraderCall = upgrader.Upgrade

func getMessage(serverState Sensors, clientState Sensors, subscriptions map[int]bool) ([]byte, Sensors, int) {
	diff, newState := ComputeStateDiff(serverState, clientState, subscriptions)
	msg, _ := json.Marshal(diff)
	return msg, newState, len(diff)
}

type WebSocket interface {
	IsDead() bool
	IsWriting() bool
	SendState(state Sensors)
	Close() error
	GetVisibility() []int
}
type Gorilla interface {
	WriteMessage(messageType int, data []byte) error
	Close() error
	ReadMessage() (messageType int, p []byte, err error)
}

type GorillaWrapper struct {
	logger     log4g.Logger
	ptr        Gorilla
	isWriting  bool
	writeLock  sync.Mutex
	err        error
	lastState  Sensors
	visibility map[int]bool
}

func (gw *GorillaWrapper) IsDead() bool {
	return gw.err != nil
}

func (gw *GorillaWrapper) GetVisibility() []int {
	ids := make([]int, len(gw.visibility))
	idx := 0
	for id, v := range gw.visibility {
		if v {
			ids[idx] = id
			idx += 1
		}
	}
	return ids[:idx]
}

func (gw *GorillaWrapper) IsWriting() bool {
	return gw.isWriting
}

func (gw *GorillaWrapper) Write(s []byte) {
	logger := gw.logger.FunCall()
	err := gw.ptr.WriteMessage(1, s)
	if err != nil {
		logger.Error("couldn't write to client", err)
		gw.err = err
	}
}

func (gw *GorillaWrapper) Close() error {
	return gw.ptr.Close()
}

func (gw *GorillaWrapper) SendStateUnsafe(state Sensors) {
	defer func() {
		gw.isWriting = false
	}()
	msg, newState, diffSize := getMessage(state, gw.lastState, gw.visibility)
	if diffSize > 0 {
		gw.Write(msg)
		gw.lastState = newState
	}
}

func (gw *GorillaWrapper) SendState(state Sensors) {
	logger := gw.logger.FunCall()
	logger.Info("sending data for", gw.visibility, "sensors")
	go func() {
		gw.writeLock.Lock()
		defer gw.writeLock.Unlock()
		if !(gw.IsWriting() || gw.IsDead()) {
			gw.isWriting = true
			go gw.SendStateUnsafe(state)
		}
	}()
}

func (gw *GorillaWrapper) listenToWebSocket() {
	logger := gw.logger.FunCall(gw)
	for {
		_, message, err := gw.ptr.ReadMessage()
		if err != nil {
			gw.err = err
			logger.Error(err)
			break
		}
		visibility := map[string]bool{}
		err = json.Unmarshal(message, &visibility)
		logger.Info("received message", string(message))
		if err != nil {
			gw.err = err
			logger.Error(err)
		} else {
			newVisibility := make(map[int]bool)
			logger.Info("parsed visibility", visibility)
			for idStr, v := range visibility {
				id, err := strconv.ParseInt(idStr, 10, 32)
				if err == nil && v {
					newVisibility[int(id)] = v
				} else if err != nil {
					logger.Error("parsing int", idStr, err)
				}
			}
			logger.Info("visiblity changed", newVisibility)
			gw.visibility = newVisibility
		}
	}
}

func wsHandler(logger log4g.Logger) func(w http.ResponseWriter, r *http.Request) {
	logger = logger.FunCall()
	return func(w http.ResponseWriter, r *http.Request) {
		c, err := upgraderCall(w, r, nil)
		if err != nil || c == nil {
			logger.Error(err, "connections is", c)
			return
		}
		gw := GorillaWrapper{
			logger:    logger,
			isWriting: false,
			ptr:       c,
			lastState: make(Sensors),
		}
		go catch.SanitizeFunc((&gw).listenToWebSocket)()
		connections_lock.Lock()
		defer connections_lock.Unlock()
		connections = append(connections, &gw)
	}
}

func clearDeleted(logger log4g.Logger) {
	logger.FunCall()
	connections_alive := make([]WebSocket, 0)
	connections_lock.Lock()
	defer connections_lock.Unlock()
	for _, c := range connections {
		if !c.IsDead() {
			connections_alive = append(connections_alive, c)
		} else {
			c.Close()
		}
	}
	logger.Info("deleted ", len(connections_alive), len(connections))
	connections = connections_alive
}

func BroadCast(logger log4g.Logger, state Sensors) {
	logger.FunCall()
	clearDeleted(logger)
	logger.Info("broadcasting", len(state), "sensors to", len(connections), "clients")
	for _, c := range connections {
		c.SendState(state)
	}
}
