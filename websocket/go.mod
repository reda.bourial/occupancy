module gitlab.com/reda.bourial/occupancy/-/tree/main/websocket

go 1.15

require (
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/go-redis/redis/v8 v8.8.0
	github.com/go-redis/redismock/v8 v8.0.6
	github.com/go-sql-driver/mysql v1.6.0
	github.com/google/go-cmp v0.5.6 // indirect
	github.com/gorilla/websocket v1.4.2
	github.com/mattn/go-sqlite3 v1.14.9
	github.com/onsi/gomega v1.16.0 // indirect
	github.com/stretchr/testify v1.7.0
	gitlab.com/reda.bourial/catch v1.0.4
	gitlab.com/reda.bourial/log4g v1.0.2
)
