package main

import (
	"os"
	"testing"
	"time"

	redis "github.com/go-redis/redis/v8"
	"github.com/stretchr/testify/assert"
	"gitlab.com/reda.bourial/catch"
	"gitlab.com/reda.bourial/log4g"
)

func TestGetFlushFunc(t *testing.T) {
	t.Run("with state", func(t *testing.T) {
		sensors := Sensors{
			1: {
				"value": "5",
			},
		}
		getSensors = func(logger log4g.Logger, client *redis.Client) Sensors {
			return sensors
		}
		var broadCastedSensors Sensors = nil
		broadCast = func(logger log4g.Logger, state Sensors) {
			broadCastedSensors = state
		}
		getFlushFunc(log4g.MockLogger(), nil)()
		assert.Equal(t, broadCastedSensors, sensors)
	})
	t.Run("with empty state", func(t *testing.T) {
		getSensors = func(logger log4g.Logger, client *redis.Client) Sensors {
			return Sensors{}
		}
		var broadCastedSensors Sensors = nil
		broadCast = func(logger log4g.Logger, state Sensors) {
			broadCastedSensors = state
		}
		getFlushFunc(log4g.MockLogger(), nil)()
		assert.Equal(t, broadCastedSensors, Sensors{})
	})
}

func TestFlushRedisToWebSockets(t *testing.T) {
	called := false
	getFlushFunc = func(logger log4g.Logger, client *redis.Client) func() {
		return func() {
			if !called {
				called = true
			} else {
				time.Sleep(10 * time.Hour)
			}
			panic("test")
		}
	}
	go FlushRedisToWebSockets(log4g.MockLogger(), nil)
	time.Sleep(time.Second / 10)
	assert.True(t, called)
}

func TestGetRedis(t *testing.T) {
	t.Run("normal", func(t *testing.T) {
		var args *redis.Options
		getRedisClient = func(opt *redis.Options) *redis.Client {
			args = opt
			return nil
		}
		os.Setenv("REDIS_HOST", "REDIS_HOST")
		os.Setenv("REDIS_PORT", "REDIS_PORT")
		os.Setenv("REDIS_PW", "REDIS_PW")
		os.Setenv("CACHE_REDIS_DB", "0")
		ret := GetRedis()
		assert.Nil(t, ret)
		assert.Equal(t, args.Addr, "REDIS_HOST:REDIS_PORT")
		assert.Equal(t, args.Password, "REDIS_PW")
		assert.Equal(t, args.DB, 0)
	})
	t.Run("fails", func(t *testing.T) {
		os.Setenv("REDIS_HOST", "REDIS_HOST")
		os.Setenv("REDIS_PORT", "REDIS_PORT")
		os.Setenv("REDIS_PW", "REDIS_PW")
		os.Setenv("CACHE_REDIS_DB", "TOTO")
		panicked, err := catch.Panic(func() {
			GetRedis()
		})
		assert.True(t, panicked)
		assert.NotNil(t, err)
	})
}

func TestMain(t *testing.T) {
	os.Setenv("CACHE_REDIS_DB", "0")
	getRedisClient = func(opt *redis.Options) *redis.Client {
		return nil
	}
	calledFlushRedisToWebSockets := false
	flushRedisToWebSockets = func(logger log4g.Logger, client *redis.Client) {
		calledFlushRedisToWebSockets = true
	}
	go main()
	time.Sleep(time.Second / 10)
	assert.True(t, calledFlushRedisToWebSockets)
}
