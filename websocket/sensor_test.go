package main

import (
	"fmt"
	"sort"
	"testing"

	"github.com/go-redis/redismock/v8"
	"github.com/stretchr/testify/assert"
	"gitlab.com/reda.bourial/log4g"
)

func TestMapToArray(t *testing.T) {
	ret := mapToArray(map[int]bool{
		1: true,
		3: false,
		5: true,
		7: false,
		9: false,
	})
	sort.Ints(ret)
	assert.Equal(t, ret, []int{1, 5})
}

func TestGetSensorIds(t *testing.T) {
	connections = []WebSocket{
		&GorillaWrapper{
			err: fmt.Errorf("some error"),
			visibility: map[int]bool{
				42: true,
				43: false,
			},
		},
		&GorillaWrapper{
			visibility: map[int]bool{
				42: false,
				41: true,
				45: true,
				43: false,
			},
		},
	}
	ids := GetSensorIds()
	sort.Ints(ids)
	assert.Equal(t, ids, []int{41, 45})
}

func TestGetSensors(t *testing.T) {
	connections = []WebSocket{
		&GorillaWrapper{
			visibility: map[int]bool{
				41: true,
			},
		},
	}
	db, mock := redismock.NewClientMock()
	mock.ExpectGet("sensor_in_unverified_41").SetVal("hello world")
	sensors := GetSensors(log4g.MockLogger(), db)
	assert.Equal(t, sensors, Sensors{
		41: Count{
			"in_unverified": "hello world",
		},
	})
}

func TestDeepCopy(t *testing.T) {
	subs := map[int]bool{
		41: true,
	}
	sensors := Sensors{
		41: Count{
			"in_unverified": "hello world",
		},
		42: Count{
			"in_unverified": "hello world",
		},
	}
	copy := sensors.DeepCopy(subs)
	assert.Equal(t, copy, Sensors(
		Sensors{
			41: Count{"in_unverified": "hello world"},
		}))
}

func TestComputeStateDiff(t *testing.T) {
	target := Sensors{
		41: Count{
			"in_unverified": "hello world",
		},
		42: Count{
			"in_unverified":  "hello world",
			"out_unverified": "hello !",
		},
	}
	current := Sensors{
		41: Count{
			"in_unverified": "hello world",
		},
		42: Count{
			"in_unverified": "hello world",
			"something":     "world !",
		},
	}
	t.Run("not in subs", func(t *testing.T) {
		subs := map[int]bool{
			41: true,
		}
		diff, newState := ComputeStateDiff(target, current, subs)
		assert.Equal(t, diff, Sensors{})
		assert.Equal(t, newState, Sensors{41: Count{
			"in_unverified": "hello world",
		}})
	})
	t.Run("in subs", func(t *testing.T) {
		subs := map[int]bool{
			41: true,
			42: true,
		}
		diff, newState := ComputeStateDiff(target, current, subs)
		assert.Equal(t, diff, Sensors{42: Count{"out_unverified": "hello !"}})
		assert.Equal(t, newState, Sensors{
			41: Count{"in_unverified": "hello world"},
			42: Count{"in_unverified": "hello world", "out_unverified": "hello !"}})
	})
}
