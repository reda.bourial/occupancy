package main

import (
	"fmt"

	redis "github.com/go-redis/redis/v8"
	"gitlab.com/reda.bourial/log4g"
)

var keys = []string{
	"in_unverified",
	"out_unverified",
	"occupancy_unverified",
	"state_verified",
}
var keys_len = len(keys)

type Count map[string]string

func (c Count) Diff(otherC Count) Count {
	diff := make(Count)
	for k, v := range c {
		if otherC[k] != v {
			diff[k] = v
		}
	}
	return diff
}

func GetCount(sensorId int, logger log4g.Logger, client *redis.Client) Count {
	logger = logger.FunCall(sensorId)
	count := make(Count)
	type entry struct {
		key   string
		value string
		err   error
	}
	results := make(chan entry)
	for _, key := range keys {
		go func(key string) {
			redis_key := fmt.Sprintf("sensor_%s_%d", key, sensorId)
			value, err := client.Get(ctx, redis_key).Result()
			results <- entry{
				key:   key,
				value: value,
				err:   err,
			}

		}(key)
	}
	nbReturns := 0
	for nbReturns < keys_len {
		result := <-results
		if result.err != nil {
			logger.Error(result.err)
		} else {
			count[result.key] = result.value
		}
		nbReturns += 1
	}
	logger.Info(count)
	return count
}

type Sensors map[int]Count

func mapToArray(idsMap map[int]bool) []int {
	ids := make([]int, len(idsMap))
	idx := 0
	for id, v := range idsMap {
		if v {
			ids[idx] = id
			idx += 1
		}
	}
	return ids[:idx]
}

func GetSensorIds() []int {
	idsMap := make(map[int]bool)
	connections_lock.Lock()
	defer connections_lock.Unlock()
	for _, c := range connections {
		if !c.IsDead() {
			for _, id := range c.GetVisibility() {
				idsMap[id] = true
			}
		}
	}
	return mapToArray(idsMap)
}

type SensorEntry struct {
	value Count
	key   int
}

func GetSensors(logger log4g.Logger, client *redis.Client) Sensors {
	ids := GetSensorIds()
	sensors := make(Sensors)
	logger.Info("listening for sensors", ids)
	done := make(chan (SensorEntry))
	for _, id := range ids {
		go func(id int) {
			value := GetCount(id, logger, client)
			done <- SensorEntry{
				key:   id,
				value: value,
			}
		}(id)
	}
	goroutinesDone := 0
	for goroutinesDone < len(ids) {
		entry := <-done
		sensors[entry.key] = entry.value
		goroutinesDone += 1
	}
	return sensors
}

func (s Sensors) DeepCopy(subscriptions map[int]bool) Sensors {
	copy := make(Sensors)
	for k, v := range s {
		if subscriptions[k] {
			copy[k] = v
		}
	}
	return copy
}

func ComputeStateDiff(target Sensors, current Sensors, subscriptions map[int]bool) (diff Sensors, newState Sensors) {
	diff = make(Sensors, 0)
	newState = target.DeepCopy(subscriptions)
	for k, v := range newState {
		sensorDiff := v.Diff(current[k])
		if len(sensorDiff) > 0 {
			diff[k] = sensorDiff
		}
	}
	return diff, newState
}
