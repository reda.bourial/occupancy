package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"sync"
	"time"

	redis "github.com/go-redis/redis/v8"
	"gitlab.com/reda.bourial/catch"
	"gitlab.com/reda.bourial/log4g"
)

var addr = flag.String("addr", "0.0.0.0:7777", "http service address")
var ctx = context.Background()

var connections = make([]WebSocket, 0)
var connections_lock sync.Mutex

var getSensors = GetSensors
var broadCast = BroadCast

func GetFlushFunc(logger log4g.Logger, client *redis.Client) func() {
	return func() {
		state := getSensors(logger, client)
		logger.Info("state len:", len(state))
		if len(state) < 1 {
			logger.Info("waiting 50 milliseconds")
			time.Sleep(time.Second / 20)
		}
		broadCast(logger, state)
	}
}

var getFlushFunc = GetFlushFunc

func FlushRedisToWebSockets(logger log4g.Logger, client *redis.Client) {
	logger = logger.FunCall()
	flushFunc := getFlushFunc(logger, client)
	safeFlushFunc := catch.SanitizeFunc(flushFunc)
	for true {
		_, err := safeFlushFunc()
		if err != nil {
			logger.Error(err)
		}
	}
}

var getRedisClient = redis.NewClient

func GetRedis() *redis.Client {
	host := os.Getenv("REDIS_HOST")
	port := os.Getenv("REDIS_PORT")
	pwd := os.Getenv("REDIS_PW")
	dbStr := os.Getenv("CACHE_REDIS_DB")
	db, err := strconv.Atoi(dbStr)
	if err != nil {
		panic(err)
	}
	return getRedisClient(&redis.Options{
		Addr:     fmt.Sprintf("%s:%s", host, port),
		Password: pwd,
		DB:       db,
	})

}

var flushRedisToWebSockets = FlushRedisToWebSockets
var defaultLogger = log4g.NewConsoleLogger

func main() {
	logger := defaultLogger().PrependTime()
	client := GetRedis()
	go flushRedisToWebSockets(logger, client)
	flag.Parse()
	log.SetFlags(0)
	http.HandleFunc("/", wsHandler(logger))
	http.ListenAndServe(*addr, nil)
}
