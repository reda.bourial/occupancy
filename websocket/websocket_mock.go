package main

import "fmt"

type MockedWebsocket struct {
	writeMessageArgs  []interface{}
	writeMessageRet   error
	CloseRet          error
	readMessageRet    []byte
	readMessageCalled bool
}

func (m *MockedWebsocket) WriteMessage(messageType int, data []byte) error {
	m.writeMessageArgs = append(m.writeMessageArgs, []interface{}{messageType, data})
	return m.writeMessageRet
}

func (m *MockedWebsocket) Close() error {
	return m.CloseRet
}

func (m *MockedWebsocket) ReadMessage() (messageType int, p []byte, err error) {
	if m.readMessageCalled {
		return 0, nil, fmt.Errorf("end of messages")
	}
	m.readMessageCalled = true
	return 1, m.readMessageRet, nil
}
