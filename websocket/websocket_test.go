package main

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/gorilla/websocket"
	"github.com/stretchr/testify/assert"
	"gitlab.com/reda.bourial/log4g"
)

func TestUpgrade(t *testing.T) {
	ret := upgrader.CheckOrigin(nil)
	assert.True(t, ret)
}

func TestGetMessage(t *testing.T) {
	target := Sensors{
		42: Count{
			"in_unverified":  "hello world",
			"out_unverified": "hello !",
		},
	}
	current := Sensors{
		42: Count{
			"in_unverified": "hello world",
			"something":     "world !",
		},
	}
	subs := map[int]bool{
		42: true,
	}
	msg, newState, diff := getMessage(target, current, subs)
	assert.Equal(t, fmt.Sprint(string(msg)), "{\"42\":{\"out_unverified\":\"hello !\"}}")
	assert.Equal(t, newState, Sensors{42: Count{"in_unverified": "hello world", "out_unverified": "hello !"}})
	assert.Equal(t, diff, 1)
}

func TestIsWriting(t *testing.T) {
	wrapper := &GorillaWrapper{
		isWriting: true,
	}
	assert.True(t, wrapper.IsWriting())
}

func TestWrite(t *testing.T) {
	text := []byte("hello")
	logger := log4g.MockLogger()
	t.Run("ok", func(t *testing.T) {
		mock := MockedWebsocket{
			writeMessageRet: nil,
		}
		wrapper := &GorillaWrapper{
			ptr: &mock,
		}
		wrapper.Write(text)
		assert.Nil(t, wrapper.err)
		expected := []interface{}{[]interface{}{1, text}}
		assert.Equal(t, mock.writeMessageArgs, expected)
	})
	t.Run("err", func(t *testing.T) {
		err := fmt.Errorf("hello world")
		mock := MockedWebsocket{
			writeMessageRet: err,
		}
		wrapper := &GorillaWrapper{
			ptr:    &mock,
			logger: logger,
		}
		wrapper.Write(text)
		assert.Equal(t, wrapper.err, err)
		expected := []interface{}{[]interface{}{1, text}}
		assert.Equal(t, mock.writeMessageArgs, expected)
	})
}

func TestClose(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		mock := MockedWebsocket{
			CloseRet: nil,
		}
		wrapper := &GorillaWrapper{
			ptr: &mock,
		}
		assert.Nil(t, wrapper.Close())
	})
	t.Run("err", func(t *testing.T) {
		err := fmt.Errorf("hello world")
		mock := MockedWebsocket{
			CloseRet: err,
		}
		wrapper := &GorillaWrapper{
			ptr: &mock,
		}
		assert.Equal(t, fmt.Sprint(err), wrapper.Close().Error())
	})
}

func TestSendStateUnsafe(t *testing.T) {
	target := Sensors{
		42: Count{
			"in_unverified":  "hello world",
			"out_unverified": "hello !",
		},
	}
	current := Sensors{
		42: Count{
			"in_unverified": "hello world",
			"something":     "world !",
		},
	}
	subs := map[int]bool{
		42: true,
	}
	mock := MockedWebsocket{
		writeMessageRet: nil,
	}
	wrapper := &GorillaWrapper{
		ptr:        &mock,
		lastState:  current,
		visibility: subs,
		isWriting:  true,
	}
	wrapper.SendStateUnsafe(target)
	assert.Nil(t, wrapper.err)
	ret := []byte("{\"42\":{\"out_unverified\":\"hello !\"}}")
	expected := []interface{}{[]interface{}{1, ret}}
	assert.Equal(t, mock.writeMessageArgs, expected)
	assert.Equal(t, wrapper.isWriting, false)
	assert.Equal(t, wrapper.lastState, Sensors{42: Count{"in_unverified": "hello world", "out_unverified": "hello !"}})
}

func TestSendState(t *testing.T) {
	target := Sensors{
		42: Count{
			"in_unverified":  "hello world",
			"out_unverified": "hello !",
		},
	}
	current := Sensors{
		42: Count{
			"in_unverified": "hello world",
			"something":     "world !",
		},
	}
	subs := map[int]bool{
		42: true,
	}
	mock := MockedWebsocket{
		writeMessageRet: nil,
	}
	wrapper := &GorillaWrapper{
		ptr:        &mock,
		lastState:  current,
		visibility: subs,
		isWriting:  false,
		logger:     log4g.MockLogger(),
	}
	wrapper.SendState(target)
	time.Sleep(time.Second / 100)
	assert.Nil(t, wrapper.err)
	expected := []interface{}{1, []byte("{\"42\":{\"out_unverified\":\"hello !\"}}")}
	assert.Equal(t, mock.writeMessageArgs[0], expected)
	assert.Equal(t, wrapper.isWriting, false)
	assert.Equal(t, wrapper.lastState, Sensors{42: Count{"in_unverified": "hello world", "out_unverified": "hello !"}})
}

func TestListenToWebSocket(t *testing.T) {
	t.Run("Works", func(t *testing.T) {
		subs := map[int]bool{
			45: true,
		}
		mock := MockedWebsocket{
			readMessageRet: []byte("{\"52\":true}"),
		}
		wrapper := &GorillaWrapper{
			ptr:        &mock,
			visibility: subs,
			isWriting:  false,
			logger:     log4g.MockLogger(),
		}
		wrapper.listenToWebSocket()
		assert.NotNil(t, wrapper.err)
		expected := map[int]bool{
			52: true,
		}
		assert.Equal(t, wrapper.visibility, expected)
	})
	t.Run("ignores malformed int", func(t *testing.T) {
		subs := map[int]bool{
			45: true,
		}
		mock := MockedWebsocket{
			readMessageRet: []byte("{\"5a2\":true}"),
		}
		wrapper := &GorillaWrapper{
			ptr:        &mock,
			visibility: subs,
			isWriting:  false,
			logger:     log4g.MockLogger(),
		}
		wrapper.listenToWebSocket()
		assert.NotNil(t, wrapper.err)
		expected := map[int]bool{}
		assert.Equal(t, wrapper.visibility, expected)
	})
	t.Run("quits on malformed message", func(t *testing.T) {
		subs := map[int]bool{
			45: true,
		}
		mock := MockedWebsocket{
			readMessageRet: []byte("helloworld"),
		}
		wrapper := &GorillaWrapper{
			ptr:        &mock,
			visibility: subs,
			isWriting:  false,
			logger:     log4g.MockLogger(),
		}
		wrapper.listenToWebSocket()
		assert.NotNil(t, wrapper.err)
		assert.Equal(t, wrapper.visibility, subs)
	})
	t.Run("quits on error", func(t *testing.T) {
		subs := map[int]bool{
			45: true,
		}
		mock := MockedWebsocket{
			readMessageCalled: true,
		}
		wrapper := &GorillaWrapper{
			ptr:        &mock,
			visibility: subs,
			isWriting:  false,
			logger:     log4g.MockLogger(),
		}
		wrapper.listenToWebSocket()
		assert.NotNil(t, wrapper.err)
		assert.Equal(t, wrapper.visibility, subs)
	})
}

func TestWsHandler(t *testing.T) {
	t.Run("Works", func(t *testing.T) {
		connections = []WebSocket{}
		logger := log4g.MockLogger()
		req := httptest.NewRequest("GET", "ws://websocket", nil)
		res := httptest.NewRecorder()
		upgraderCall = func(w http.ResponseWriter, r *http.Request, responseHeader http.Header) (*websocket.Conn, error) {
			return &websocket.Conn{}, nil
		}
		wsHandler(logger)(res, req)
		assert.Equal(t, len(connections), 1)
		connections = []WebSocket{}
	})
	t.Run("Drop on error", func(t *testing.T) {
		connections = []WebSocket{}
		logger := log4g.MockLogger()
		req := httptest.NewRequest("GET", "ws://websocket", nil)
		res := httptest.NewRecorder()
		upgraderCall = func(w http.ResponseWriter, r *http.Request, responseHeader http.Header) (*websocket.Conn, error) {
			return &websocket.Conn{}, fmt.Errorf("testErr")
		}
		wsHandler(logger)(res, req)
		assert.Equal(t, len(connections), 0)
		connections = []WebSocket{}
	})
}

func TestClearDeleted(t *testing.T) {
	err := fmt.Errorf("test")
	connections = []WebSocket{
		&GorillaWrapper{
			err: nil,
		},
		&GorillaWrapper{
			err: err,
			ptr: &MockedWebsocket{
				CloseRet: nil,
			},
		},
	}
	clearDeleted(log4g.MockLogger())
	assert.Equal(t, len(connections), 1)
	connections = []WebSocket{}
}

func TestBroadCast(t *testing.T) {
	ws := MockedWebsocket{
		CloseRet: nil,
	}
	connections = []WebSocket{
		&GorillaWrapper{
			ptr: &ws,
			visibility: map[int]bool{
				3: true,
				1: false,
			},
			logger: log4g.MockLogger(),
		},
	}
	BroadCast(log4g.MockLogger(), Sensors{
		1: {
			"k1": "v1",
		},
		3: {
			"k1": "v1",
		},
	})
	time.Sleep(time.Second / 10)
	assert.Equal(t, len(ws.writeMessageArgs), 1)
	connections = []WebSocket{}
}
