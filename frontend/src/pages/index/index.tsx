import React from 'react';

import { Typography, Row } from 'antd';
const { Title } = Typography;

import SensorList from '@/components/sensor-list';
import AddSensorModal from '@/components/sensor-modal';
import WebsocketStatus from '@/components/websocket-status';

import styles from './index.less';

import { useSelector } from 'react-redux';

function IndexPage({ className }) {
  return (
    <div className={className}>
      <Row>
        <Title className={styles.title}>Sensors </Title>
        <AddSensorModal btnClass={styles.addBtn} iconClass={styles.iconClass} />
      </Row>
      <Row>
        <WebsocketStatus />
      </Row>
      <SensorList />
    </div>
  );
}

export default IndexPage;
