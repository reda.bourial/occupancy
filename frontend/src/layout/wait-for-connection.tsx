import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

function WaitForInit({ children }) {
  const dispatch = useDispatch();
  const loaded = useSelector(({ app }) => app.loaded);

  React.useEffect(() => {
    dispatch({
      type: `app/init`,
      payload: {
        dispatch,
      },
    });
  }, []);

  if (!loaded) {
    return <>Loading please wait ...</>;
  }

  return <>{children}</>;
}

export default WaitForInit;
