import React from 'react';
import styles from './padded-body.less';

function PaddedBody({ children }) {
  return (
    <div className={[styles.paddedBody, styles.coloredBody].join(' ')}>
      {children}
    </div>
  );
}

export default PaddedBody;
