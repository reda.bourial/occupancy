import React, { useState, useMemo } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { Card, Col, Row, Typography, Space, Statistic } from 'antd';
import { SearchOutlined, SendOutlined } from '@ant-design/icons';

import { sanitizeState } from '@/services/common';
import SendObservationModal from './send-observation-modal';
import GetOccupancyModal from './get-occupancy';
import styles from './sensor-card.less';
import VisibilitySensor from 'react-visibility-sensor';
import Metrics from './metrics/metrics';

export default function SensorCard({ sensorId }) {
  const unsafeSensor = useSelector(({ app: { sensors } }) => sensors[sensorId]);
  const sensor = sanitizeState(unsafeSensor);
  const { name, counts } = sensor;
  const dispatch = useDispatch();
  const title = <Typography.Title level={4}>{name}</Typography.Title>;
  return (
    <VisibilitySensor
      partialVisibility={true}
      delayedCall={true}
      onChange={(visibility) =>
        dispatch({
          type: 'app/handleVisibilityChange',
          payload: { id: sensorId, visibility },
        })
      }
    >
      <Card
        actions={[
          <GetOccupancyModal
            sensor={sensor}
            button={<SearchOutlined className={styles.sensorAction} />}
          />,
          <SendObservationModal
            sensor={sensor}
            button={<SendOutlined className={styles.sensorAction} />}
          />,
        ]}
        className={styles.sensorCard}
        title={title}
      >
        <Metrics counts={counts} />
      </Card>
    </VisibilitySensor>
  );
}
