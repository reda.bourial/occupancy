import React from 'react';
import { Col, Statistic } from 'antd';

const getTurnOver = (incoming, outgoing) =>
  Math.floor((outgoing + incoming) / 2);

export default function InnerMetrics({ incoming, outgoing }) {
  const occupancy = incoming - outgoing;
  const turnOver = getTurnOver(incoming, outgoing);
  return (
    <>
      <Col span={6}>
        <Statistic title="Occupancy" value={occupancy} />
      </Col>
      <Col span={6}>
        <Statistic title="Turner Over" value={turnOver} />
      </Col>
      <Col span={6}>
        <Statistic title="Cumulative Ins" value={incoming} />
      </Col>
      <Col span={6}>
        <Statistic title="Cumulative Outs" value={outgoing} />
      </Col>
    </>
  );
}
