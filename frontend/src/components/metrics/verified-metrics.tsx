import React, { useMemo } from 'react';

import { Col, Row, Typography, Statistic } from 'antd';
import InnerMetrics from './inner-metrics';

export default function VerifiedMetrics({
  lastObservation,
  incoming,
  outgoing,
  latency,
}) {
  const timezoneOffset = new Date().getTimezoneOffset() * 60000;
  const getTotalValue = (): Number => {
    if (lastObservation) {
      try {
        return new Number(
          new Date() - new Date(lastObservation) + timezoneOffset,
        );
      } catch (e) {}
    }
    return 0;
  };
  const totalLatency = useMemo(getTotalValue, [lastObservation]);
  const wsLatency = totalLatency - latency / 1000;
  // ugly workaround for timezone
  const prettyDate = new Date(
    lastObservation ? new Date(lastObservation) - timezoneOffset : null,
  )
    .toISOString()
    .substr(0, 21)
    .replace('T', ' ');
  return (
    <>
      <Typography.Title level={5}>
        Verified metrics (consistent but latency is bad)
      </Typography.Title>
      <Row>
        <InnerMetrics incoming={incoming} outgoing={outgoing} />
        <Col span={8}>
          <Statistic title="Last observation (UTC)" value={prettyDate} />
        </Col>
        <Col span={5}>
          <Statistic
            title="Display latency"
            value={Math.floor(wsLatency)}
            suffix="ms"
          />
        </Col>
        <Col span={5}>
          <Statistic
            title="Compute latency"
            value={Math.floor(latency / 1000)}
            suffix="ms"
          />
        </Col>
        <Col span={6}>
          <Statistic
            title="Total latency"
            value={Math.floor(totalLatency)}
            suffix="ms"
          />
        </Col>
      </Row>
    </>
  );
}
