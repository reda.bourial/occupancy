import React from 'react';

import UnverifiedMetrics from './unverified-metrics';
import VerifiedMetrics from './verified-metrics';

export default function Metrics({ counts }) {
  const { state_verified } = counts;
  const getIndex = (idx) => {
    if (state_verified) {
      return state_verified[idx];
    }
    return 0;
  };
  const getIndexAsNumber = (idx) => Number(getIndex(idx));
  return (
    <>
      <UnverifiedMetrics
        outgoing={Number(counts?.out_unverified)}
        incoming={Number(counts?.in_unverified)}
      />
      <VerifiedMetrics
        lastObservation={getIndex(0)}
        incoming={getIndexAsNumber(1)}
        outgoing={getIndexAsNumber(2)}
        latency={getIndexAsNumber(3)}
      />
    </>
  );
}
