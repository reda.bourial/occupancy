import React from 'react';

import { Row, Typography } from 'antd';
import InnerMetrics from './inner-metrics';

export default function UnverifiedMetrics({ incoming, outgoing }) {
  return (
    <>
      <Typography.Title level={5}>
        Unverified metrics (best latency but unconsistent view):
      </Typography.Title>
      <Row>
        <InnerMetrics incoming={incoming} outgoing={outgoing} />
      </Row>
    </>
  );
}
