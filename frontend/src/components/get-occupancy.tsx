import React, { useState } from 'react';
import Modal from '@/components/utils/modal';
import { getObservation } from '@/services/observation';
import { Form, Card, Col, DatePicker, Row, Space } from 'antd';
import moment from 'moment';

const GetOccupancyModal = ({ sensor, button, modalClass, datePickerClass }) => {
  const [response, setResponse] = useState(null);

  const onChange = async (ts) => {
    const date = new Date(
      new Date(ts) - new Date().getTimezoneOffset() * 60000,
    );
    return await getObservation(sensor.name, date).then(setResponse);
  };

  const formItems = (
    <Form
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 16 }}
      initialValues={{ ts: moment(new Date(new Date() - 15)) }}
    >
      <Form.Item
        label="Date"
        name="ts"
        rules={[{ required: true, message: 'Please input your date!' }]}
      >
        <DatePicker
          className={datePickerClass}
          onChange={onChange}
          showTime={true}
          format="YYYY-MM-DD HH:mm:ss"
        />
      </Form.Item>
    </Form>
  );

  return (
    <>
      <Modal
        button={button}
        modalClass={modalClass}
        modalTitle={`Get occupancy for ${sensor.name}`}
        okButtonProps={{ style: { display: 'none' } }}
        cancelButtonProps={{ style: { display: 'none' } }}
      >
        <Space direction={'horizontal'} />
        <Row>{formItems}</Row>
        {response ? (
          <Card>
            {Object.entries(response).map(([k, v]) => (
              <Row key={`row_${k}`}>
                <Col span={12}>{k}</Col>
                <Col span={12}>{v}</Col>
              </Row>
            ))}
          </Card>
        ) : null}
      </Modal>
    </>
  );
};

export default GetOccupancyModal;
