import React, { useState } from 'react';
import styles from './sensor-modal.less';
import { sendObservation } from '@/services/observation';
import { message, Form, Input, Select, DatePicker, InputNumber } from 'antd';

import Modal from '@/components/utils/modal';

const SendObservationModal = ({
  sensor: { name },
  button,
  datePickerClass,
  inInputClass,
  outInputClass,
  modalClass,
}) => {
  const [date, setDate] = useState(null);
  const [incoming, setIncoming] = useState(0);
  const [outgoing, setOutgoing] = useState(0);

  const reset = () => {
    setDate(null);
    setIncoming(0);
    setOutgoing(0);
  };

  const processResponse = (r) => {
    const e = r?.error;
    const callback = e ? message.error : message.success;
    callback(`Observation sent : ${JSON.stringify(r)}`);
  };

  const handleOk = () =>
    sendObservation({
      name,
      in: incoming,
      out: outgoing,
      ts: date.toISOString(),
    })
      .then(processResponse)
      .then(reset);

  const formItems = (
    <Form
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 16 }}
      initialValues={{ name }}
    >
      <Form.Item
        label="Name"
        name="name"
        rules={[{ required: true, message: 'Please input your sensor name!' }]}
      >
        <Select
          placeholder="Select a option and change input text above"
          disabled={true}
        >
          <Select.Option key={`sensor_select`} value={name}>
            {' '}
            {name}{' '}
          </Select.Option>
          )
        </Select>
      </Form.Item>
      <Form.Item
        label="Timestamp"
        name="ts"
        rules={[{ required: true, message: 'Please input your date!' }]}
      >
        <DatePicker
          className={datePickerClass}
          onChange={(ts) => setDate(new Date(ts))}
          showTime={true}
          format="YYYY-MM-DD HH:mm:ss"
        />
      </Form.Item>
      <Form.Item
        label="In"
        name="in"
        rules={[{ required: true, message: 'Please input your in!' }]}
      >
        <InputNumber className={inInputClass} min={0} onChange={setIncoming} />
      </Form.Item>
      <Form.Item
        label="Out"
        name="out"
        rules={[{ required: true, message: 'Please input your out!' }]}
      >
        <InputNumber className={outInputClass} min={0} onChange={setOutgoing} />
      </Form.Item>
    </Form>
  );

  return (
    <Modal
      button={button}
      modalClass={modalClass}
      modalTitle={`Send observation as ${name}`}
      handleOk={handleOk}
    >
      {formItems}
    </Modal>
  );
};

export default SendObservationModal;
