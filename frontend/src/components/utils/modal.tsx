import React, { useState } from 'react';
import { Modal } from 'antd';

const ModalWrapper = ({
  button,
  modalClass,
  modalTitle,
  children,
  okButtonProps,
  handleOk,
  cancelButtonProps,
}) => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const setVisibility = (val) => () => setIsModalVisible(val);
  const content = isModalVisible ? (
    <Modal
      title={modalTitle}
      visible={isModalVisible}
      okButtonProps={okButtonProps}
      cancelButtonProps={cancelButtonProps}
      className={modalClass}
      onCancel={setVisibility(false)}
      onOk={() => {
        handleOk();
        setIsModalVisible(false);
      }}
    >
      {children}
    </Modal>
  ) : null;

  return (
    <>
      <b onClick={setVisibility(true)}>{button}</b>
      {content}
    </>
  );
};

export default ModalWrapper;
