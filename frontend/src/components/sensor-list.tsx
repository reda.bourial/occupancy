import React from 'react';
import { useSelector } from 'react-redux';
import SensorCard from './sensor-card';

function SensorList({}) {
  const sensors = useSelector(({ app: { sensors } }) => Object.values(sensors));
  const render = () =>
    sensors
      .map((sensor) => {
        return <SensorCard key={sensor.id} sensorId={sensor.id} />;
      })
      .reverse();
  return React.useMemo(render, [sensors.map(({ id }) => id).join('|')]);
}

export default SensorList;
