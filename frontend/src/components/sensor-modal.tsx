import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import styles from './sensor-modal.less';

import { message, Form, Input, Button } from 'antd';
import { AppstoreAddOutlined } from '@ant-design/icons';

import Modal from '@/components/utils/modal';

const AddSensorModal = ({
  btnClass,
  iconClass,
  modalClass,
  nameInputClass,
}) => {
  const dispatch = useDispatch();
  const [form] = Form.useForm();
  const [name, setName] = useState('');

  const afterCreate = (resp) => {
    message.info(`creating sensor ...`);
    if (resp.id) {
      dispatch({
        type: 'app/refresh',
      }).then(() => {
        message.success(`sensor ${resp.name} created`);
      });
    } else {
      message.error('something went wrong maybe you should change the name.');
    }
  };

  const create = (name) =>
    dispatch({
      type: 'sensor/create',
      payload: { name },
    });

  const handleOk = () => {
    if (name) {
      create(name).then(afterCreate);
    } else {
      message.info('empty name ignored');
    }
  };

  const formItems = (
    <Form form={form}>
      <Form.Item
        label="Name"
        name="name"
        rules={[{ required: true, message: 'Please input your sensor name!' }]}
      >
        <Input
          className={nameInputClass}
          onChange={(e) => setName(e.target.value)}
        />
      </Form.Item>
    </Form>
  );

  return (
    <Modal
      button={
        <Button className={btnClass} type="primary">
          <AppstoreAddOutlined className={iconClass} />
          Add sensor
        </Button>
      }
      modalClass={modalClass}
      modalTitle={`Add Sensor`}
      handleOk={handleOk}
    >
      {formItems}
    </Modal>
  );
};

export default AddSensorModal;
