import React from 'react';
import { useSelector } from 'react-redux';
import styles from './sensor-modal.less';

import { Row } from 'antd';

const WebsocketStatus = () => {
  const status = useSelector(({ app: { websocket } }) => websocket.status);
  return <Row>Websocket is {status}</Row>;
};

export default WebsocketStatus;
