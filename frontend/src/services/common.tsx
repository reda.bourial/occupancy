import request from 'umi-request';

request.extendOptions({
  errorHandler: ({ response }) => response,
});

export default request;

// Function for handling state_verified
// corner case
export function sanitizeState(sensor) {
  if (!sensor.counts) {
    sensor.counts = {};
  }
  const stateVerified = sensor?.counts?.state_verified;
  if (!stateVerified || typeof stateVerified === 'string') {
    try {
      sensor.counts.state_verified = JSON.parse(stateVerified);
    } catch (e) {
      sensor.counts.state_verified = [null, null, null, null];
    }
  }
  return sensor;
}
