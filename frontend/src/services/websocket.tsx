export default class WebsocketWrapper {
  socket: WebSocket;
  dispatch: Function;
  status: string = 'not started';

  constructor(url: string, dispatch: Function) {
    this.socket = new WebSocket(url);
    this.dispatch = dispatch;
    this.bindCallbacks();
  }

  private bindCallbacks() {
    this.status = 'connecting';
    this.socket.onopen = () => {
      this.status = 'connected';
    };
    this.socket.onmessage = (msg) =>
      this.dispatch({
        type: 'app/handleMessage',
        payload: { msg },
      });
    this.socket.onclose = () => {
      this.status = 'disconnected';
      this.socket = new WebSocket(this.socket.url);
      this.bindCallbacks();
    };
  }

  public send(msg) {
    try {
      this.socket.send(msg);
    } catch (e) {
      //console.log(e)
    }
  }
}
