import request from '@/services/common';

export const sendObservation = (data) =>
  request.post(`/api/webhook/`, {
    data,
  });

export const getObservation = (sensorName, date) =>
  request.get(`/api/occupancy/`, {
    params: {
      sensor: sensorName,
      atInstant: date
        ? date
        : new Date(
            new Date() + new Date().getTimezoneOffset() * 60000,
          ).toISOString(),
    },
  });
