import request from '@/services/common';

export function getSensors() {
  return request.get(`/api/sensor/`);
}

export function createSensor(data) {
  return request.post(`/api/sensor/`, { data });
}
