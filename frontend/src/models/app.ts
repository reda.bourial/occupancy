import window from 'window';

import { getSensors } from '@/services/sensors';

import Websocket from '@/services/websocket';

export default {
  namespace: 'app',
  state: {
    loaded: false,
    sensors: {},
    visibility: {},
    websocket: null,
  },
  effects: {
    *init({ payload: { dispatch } }, { call, put }) {
      yield put({
        type: 'setWebsocket',
        payload: {
          websocket: new Websocket(
            `wss://${window.location.hostname}/websocket`,
            dispatch,
          ),
        },
      });
      const sensors = yield call(getSensors);
      yield put({ type: 'setSensors', payload: { sensors } });
      yield put({ type: 'setLoaded', payload: { loaded: true } });
    },
    *refresh(_, { call, put }) {
      const sensors = yield call(getSensors);
      yield put({ type: 'setSensors', payload: { sensors } });
    },
    *handleMessage({ payload: { msg } }, { put }) {
      yield put({ type: 'setMessageReceived', payload: { msg } });
    },
    *handleVisibilityChange({ payload: { id, visibility } }, { put }) {
      yield put({ type: 'setVisibilityChange', payload: { id, visibility } });
    },
  },
  reducers: {
    setMessageReceived(state, { payload: { msg } }) {
      const { data } = msg;
      const stateDiff = JSON.parse(data);
      const handleSensor = ([id, data]) =>
        Object.entries(data).map(([k, v]) => {
          try {
            state.sensors[id].counts[k] = v;
          } catch (error) {
            // ignore errors
            // console.error(error)
          }
        });
      Object.entries(stateDiff).map(handleSensor);
      // update visible components
      // that are in message
      Object.keys(stateDiff).map((id) => {
        if (state.visibility[id]) {
          state.sensors[id] = { ...state.sensors[id] };
        }
      });
      return state;
    },
    setStatus(state, { payload: { status } }) {
      state.websocketStatus = status;
      return state;
    },
    setVisibilityChange(state, { payload: { id, visibility } }) {
      state.visibility[id] = visibility;
      const visibleOnes = ([k, v]) => v;
      const toMap = (acc, [k, v]) => {
        acc[k] = v;
        return acc;
      };
      const visibleIds = Object.entries(state.visibility)
        .filter(visibleOnes)
        .reduce(toMap, {});
      state.websocket.send(JSON.stringify(visibleIds));
      return state;
    },
    setSensors(state, { payload: { sensors } }) {
      const mapById = (acc, v) => {
        acc[v.id] = v;
        return acc;
      };
      const newSensors = sensors.reduce(mapById, {});
      return {
        ...state,
        sensors: newSensors,
      };
    },
    setWebsocket(state, { payload: { websocket } }) {
      state.websocket = websocket;
      return state;
    },
    setLoaded(state, { payload: { loaded } }) {
      state.loaded = loaded;
      return state;
    },
  },
};
