import { createSensor } from '@/services/sensors';

export default {
  namespace: 'sensor',
  state: {},
  effects: {
    *create({ payload: name }, { call }) {
      const response = yield call(createSensor, name);
      return response;
    },
  },
};
