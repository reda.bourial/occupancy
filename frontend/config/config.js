import { defineConfig } from 'umi';
import BundleTracker from 'webpack-bundle-tracker';

const publicPath =
  process.env.NODE_ENV === 'production' ? '/' : '/react-assets/';

export default defineConfig({
  devtool: 'cheap-module-source-map',
  externals: {
    window: 'window',
  },
  hash: true,
  targets: {},
  chainWebpack(config) {
    const tracker = new BundleTracker({
      publicPath: publicPath,
      filename: `webpack-stats.${process.env.NODE_ENV}.json`,
    });
    config.plugin('webpack-bundle-tracker').use(tracker);
  },
  publicPath: publicPath,
  devServer: {
    port: '3000',
  },
  nodeModulesTransform: {
    type: 'none',
  },
  title: 'Occupancy',
  routes: [
    {
      path: '/',
      title: 'Sensors',
      component: '@/pages/index',
      wrappers: ['@/layout/wait-for-connection', '@/layout/padded-body'],
      exact: false,
    },
  ],
});
