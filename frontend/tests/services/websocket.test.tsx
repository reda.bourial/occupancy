import WebsocketWrapper from '@/services/websocket';

global.WebSocket = Object;

let dispatches;
beforeEach(() => {
  dispatches = [];
});

it('it opens', async () => {
  const wrapper = new WebsocketWrapper('test', (...args) =>
    dispatches.push(args),
  );
  expect(wrapper.status).toBe('connecting');
  wrapper.socket.onopen();
  expect(wrapper.status).toBe('connected');
});

it('it handle message', async () => {
  const wrapper = new WebsocketWrapper('test', (...args) =>
    dispatches.push(args),
  );
  wrapper.socket.onmessage('hello');
  const expectedDispatches = [
    [{ payload: { msg: 'hello' }, type: 'app/handleMessage' }],
  ];
  expect(dispatches).toStrictEqual(expectedDispatches);
});

it('it reconnects', async () => {
  const wrapper = new WebsocketWrapper('test', (...args) =>
    dispatches.push(args),
  );
  wrapper.socket.onclose('hello');
  expect(wrapper.status).toBe('connecting');
});

it('it sends', async () => {
  const wrapper = new WebsocketWrapper('test', (...args) =>
    dispatches.push(args),
  );
  const sentMessages = [];
  wrapper.socket.send = (...args) => sentMessages.push(args);
  wrapper.send('toto');
  expect(sentMessages).toStrictEqual([['toto']]);
});
