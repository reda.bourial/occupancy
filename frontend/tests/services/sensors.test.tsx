import request from '@/services/common';
import { createSensor, getSensors } from '@/services/sensors';

let requests;

beforeEach(() => {
  requests = [];
  jest
    .spyOn(request, 'get')
    .mockImplementation(async (...args) => requests.push(['get', ...args]));
  jest
    .spyOn(request, 'post')
    .mockImplementation(async (...args) => requests.push(['get', ...args]));
});

it('creates sensor', async () => {
  const observation = await createSensor({ test: 'lala' });
  expect(observation).toBe(1);
  const expectedReturn = [
    [
      'get',
      '/api/sensor/',
      {
        data: {
          test: 'lala',
        },
      },
    ],
  ];
  expect(requests).toStrictEqual(expectedReturn);
});

it('gets sensors', async () => {
  const observation = await getSensors();
  expect(observation).toBe(1);
  const expectedReturn = [['get', '/api/sensor/']];
  expect(requests).toStrictEqual(expectedReturn);
});
