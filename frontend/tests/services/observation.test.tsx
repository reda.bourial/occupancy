import request from '@/services/common';
import { getObservation, sendObservation } from '@/services/observation';

let requests;

beforeEach(() => {
  requests = [];
  jest
    .spyOn(request, 'get')
    .mockImplementation(async (...args) => requests.push(['get', ...args]));
  jest
    .spyOn(request, 'post')
    .mockImplementation(async (...args) => requests.push(['get', ...args]));
});

it('gets observation', async () => {
  const observation = await getObservation('test', 'lala');
  expect(observation).toBe(1);
  const expectedReturn = [
    [
      'get',
      '/api/occupancy/',
      {
        params: {
          atInstant: 'lala',
          sensor: 'test',
        },
      },
    ],
  ];
  expect(requests).toStrictEqual(expectedReturn);
});

it('gets observation without date', async () => {
  const mockDate = new Date(1466424490000);
  jest.spyOn(global, 'Date').mockImplementation(() => mockDate);
  const observation = await getObservation('test', null);
  expect(observation).toBe(1);
  const expectedReturn = [
    [
      'get',
      '/api/occupancy/',
      {
        params: {
          atInstant: '2016-06-20T12:08:10.000Z',
          sensor: 'test',
        },
      },
    ],
  ];
  expect(requests).toStrictEqual(expectedReturn);
});

it('send observation with date', async () => {
  const observation = await sendObservation('test', 'lala');
  expect(observation).toBe(1);
  const expectedReturn = [
    [
      'get',
      '/api/webhook/',
      {
        data: 'test',
      },
    ],
  ];
  expect(requests).toStrictEqual(expectedReturn);
});
