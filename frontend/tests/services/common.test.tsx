import request, { sanitizeState } from '@/services/common';

const getState = (state_verified) =>
  sanitizeState({
    counts: {
      state_verified,
    },
  });

it('shows app', async () => {
  const resp = await request.get('https://httpbin.org/status/500');
  expect(resp.status).toBe(500);
});

it('sanitizes state str', async () => {
  const sensor = getState('helloworld');
  expect(sensor).toStrictEqual({
    counts: { state_verified: [null, null, null, null] },
  });
});

it('sanitizes state null', async () => {
  const sensor = getState(null);
  expect(sensor).toStrictEqual({ counts: { state_verified: null } });
});

it('sanitizes state array', async () => {
  const sensor = getState('[1,2,3,5]');
  expect(sensor).toStrictEqual({ counts: { state_verified: [1, 2, 3, 5] } });
});

it('sanitizes empty counts', async () => {
  const sensor = sanitizeState({
    counts: {},
  });
  expect(sensor).toStrictEqual({
    counts: { state_verified: [null, null, null, null] },
  });
});

it('sanitizes with counts as obj', async () => {
  const sensor = sanitizeState({
    counts: {
      state_verified: [1, 2, 3, 5],
    },
  });
  expect(sensor).toStrictEqual({ counts: { state_verified: [1, 2, 3, 5] } });
});
