import React from 'react';
import * as ReactRedux from 'react-redux';
import Index from '@/pages/index';
import { shallow } from 'enzyme';

jest.mock('@/components/sensor-list', () => {
  return {
    __esModule: true,
    default: jest.fn(() => <div>|sensor-list|</div>),
  };
});
jest.mock('@/components/sensor-modal', () => {
  return {
    __esModule: true,
    default: jest.fn(() => <div>|sensor-modal|</div>),
  };
});

it('loads data on init', () => {
  const state = { app: { websocket: { status: 'in test' } } };
  jest
    .spyOn(ReactRedux, 'useSelector')
    .mockImplementationOnce((fn) => fn(state));
  const container = shallow(<Index />).render();
  expect(container.text()).toBe(
    'Sensors |sensor-modal|Websocket is in test|sensor-list|',
  );
});
