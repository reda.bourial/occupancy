import app from '@/models/app';
import { getSensors } from '@/services/sensors';

jest.mock(
  'window',
  () => ({
    location: {
      hostname: 'test.com',
    },
  }),
  { virtual: true },
);

let dispatches;
beforeEach(() => {
  dispatches = [];
});

const testSensors = [
  {
    id: 42,
    name: 'f1',
  },
  {
    id: 69,
    name: 'f2',
  },
];

it('init', async () => {
  const call = jest.fn(function* (...args) {
    yield testSensors;
  });
  const put = jest.fn(() => null);
  const gen = app.effects.init(
    { payload: { dispatch: dispatches.push } },
    { call, put },
  );
  for (const _ of gen);
  expect(call.mock.calls).toEqual([[getSensors]]);
  expect(put.mock.calls.length).toEqual(3);
  expect(put.mock.calls[0][0].type).toEqual('setWebsocket');
  expect(put.mock.calls[1][0].type).toEqual('setSensors');
  expect(put.mock.calls[2][0].type).toEqual('setLoaded');
});

it('refresh', async () => {
  const call = jest.fn(function* (...args) {
    yield testSensors;
  });
  const put = jest.fn(() => null);
  const gen = app.effects.refresh({}, { call, put });
  for (const _ of gen);
  expect(call.mock.calls).toEqual([[getSensors]]);
  expect(put.mock.calls.length).toEqual(1);
  expect(put.mock.calls[0][0].type).toEqual('setSensors');
});

it('handles messages', async () => {
  const put = jest.fn(() => null);
  const gen = app.effects.handleMessage(
    { payload: { msg: 'helloworld !' } },
    { put },
  );
  for (const _ of gen);
  expect(put.mock.calls.length).toEqual(1);
  expect(put.mock.calls[0][0].type).toEqual('setMessageReceived');
  expect(put.mock.calls[0][0].payload.msg).toEqual('helloworld !');
});

it('handles visiblity change', async () => {
  const put = jest.fn(() => null);
  const payload = { id: 'helloworld !', visibility: 'never gonna give you up' };
  const gen = app.effects.handleVisibilityChange({ payload }, { put });
  for (const _ of gen);
  expect(put.mock.calls.length).toEqual(1);
  expect(put.mock.calls[0][0].type).toEqual('setVisibilityChange');
  expect(put.mock.calls[0][0].payload).toEqual(payload);
});

it('handle message received for existing sensor', async () => {
  const msg = {
    42: {
      in_unverified: '426',
      out_unverified: '461',
      occupancy_unverified: '-35',
      state_verified: '["2021-11-08T18:45:14.954311",424,454,383606]',
    },
  };
  const state = app.reducers.setMessageReceived(
    {
      sensors: {
        42: {
          counts: {},
        },
      },
      visibility: {
        42: true,
      },
    },
    {
      payload: {
        msg: {
          data: JSON.stringify(msg),
        },
      },
    },
  );
  expect(state).toEqual({
    sensors: {
      '42': {
        counts: {
          in_unverified: '426',
          occupancy_unverified: '-35',
          out_unverified: '461',
          state_verified: '["2021-11-08T18:45:14.954311",424,454,383606]',
        },
      },
    },
    visibility: {
      '42': true,
    },
  });
});

it('handle message received for unexisting sensor', async () => {
  const msg = {
    43: {
      in_unverified: '426',
    },
  };
  const state = app.reducers.setMessageReceived(
    {
      sensors: {},
      visibility: {},
    },
    {
      payload: {
        msg: {
          data: JSON.stringify(msg),
        },
      },
    },
  );
  expect(state).toEqual({ sensors: {}, visibility: {} });
});

it('sets status', async () => {
  const state = app.reducers.setStatus(
    {
      websocketStatus: 'something',
    },
    {
      payload: {
        status: 'else',
      },
    },
  );
  expect(state).toEqual({
    websocketStatus: 'else',
  });
});

it('handle visibility Change from false to true', async () => {
  const wsMessages = [];
  const state = app.reducers.setVisibilityChange(
    {
      websocket: {
        send: (v) => wsMessages.push(v),
      },
      visibility: {},
    },
    {
      payload: {
        id: 42,
        visibility: true,
      },
    },
  );
  expect(state.visibility).toStrictEqual({ 42: true });
  expect(wsMessages).toStrictEqual(['{"42":true}']);
});

it('handle visibility Change from true to false', async () => {
  const wsMessages = [];
  const state = app.reducers.setVisibilityChange(
    {
      websocket: {
        send: (v) => wsMessages.push(v),
      },
      visibility: {
        42: true,
      },
    },
    {
      payload: {
        id: 42,
        visibility: false,
      },
    },
  );
  expect(state.visibility).toStrictEqual({ 42: false });
  expect(wsMessages).toStrictEqual(['{}']);
});

it('set Sensors', async () => {
  const state = app.reducers.setSensors(
    {
      toto: 'something',
    },
    {
      payload: {
        sensors: testSensors,
      },
    },
  );
  expect(state).toEqual({
    sensors: {
      42: {
        id: 42,
        name: 'f1',
      },
      69: {
        id: 69,
        name: 'f2',
      },
    },
    toto: 'something',
  });
});

it('set websocket', async () => {
  const state = app.reducers.setWebsocket(
    {
      websocket: 'something',
    },
    {
      payload: {
        websocket: 'somethign else',
      },
    },
  );
  expect(state).toEqual({ websocket: 'somethign else' });
});

it('set Loaded', async () => {
  const state = app.reducers.setLoaded(
    {
      toto: 'something',
    },
    {
      payload: {
        loaded: 'somethign else',
      },
    },
  );
  expect(state).toEqual({ loaded: 'somethign else', toto: 'something' });
});
