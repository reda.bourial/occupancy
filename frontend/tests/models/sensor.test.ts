import sensor from '@/models/sensor';
import { createSensor } from '@/services/sensors';

it('creates sensor', async () => {
  const call = jest.fn(function* (...args) {
    yield args;
  });
  const put = jest.fn(() => null);
  const gen = sensor.effects.create(
    {
      payload: {
        name: 'test',
      },
    },
    { call, put },
  );
  for (const x of gen);
  expect(call.mock.calls.length).toBe(1);
  expect(String(call.mock.calls[0][0])).toStrictEqual(String(createSensor));
  expect(call.mock.calls[0][1]).toEqual({ name: 'test' });
  expect(put.mock.calls).toEqual([]);
});
