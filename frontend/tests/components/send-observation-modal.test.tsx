import React from 'react';
import { shallow } from 'enzyme';
import SendObservationModal from '@/components/send-observation-modal';

jest.mock('@/components/utils/modal', () => {
  return {
    __esModule: true,
    default: jest.fn((props) => {
      return (
        <div>
          {props.children}
          <button id="handleOk" onClick={props.handleOk} />
        </div>
      );
    }),
  };
});

jest.mock('@/services/observation', () => {
  return {
    __esModule: true,
    sendObservation: jest.fn(async (...args) => {
      return mockSendObservation(...args);
    }),
  };
});

jest.mock('antd', () => {
  return {
    __esModule: true,
    ...jest.requireActual('antd'),
    message: {
      error: jest.fn(async (...args) => {
        return mockMessage('error', ...args);
      }),
      success: jest.fn(async (...args) => {
        return mockMessage('success', ...args);
      }),
    },
  };
});

let mockCalls;
let mockMessage;
let mockMessages;
let mockSendObservation;

beforeEach(() => {
  mockCalls = [];
  mockMessages = [];
  mockSendObservation = (...args) => mockCalls.push(args);
  mockMessage = (...args) => mockMessages.push(args);
});

it('Sends observations', async () => {
  const container = shallow(
    <SendObservationModal
      sensor={{ id: 42, name: 'toto' }}
      datePickerClass="datePickerClass"
      inInputClass="inInputClass"
      outInputClass="outInputClass"
      modalClass="modalClass"
    />,
  );
  expect(container.render().text()).toBe('Name toto TimestampInOut');
  await container.find('.datePickerClass').prop('onChange')(
    new Date(1000000000000),
  );
  await container.find('.inInputClass').prop('onChange')(42);
  await container.find('.outInputClass').prop('onChange')(5);
  await container.dive().find('#handleOk').prop('onClick')();
  expect(mockCalls.length).toBe(1);
  expect(mockCalls[0]).toStrictEqual([
    { in: 42, name: 'toto', out: 5, ts: '2001-09-09T01:46:40.000Z' },
  ]);
  expect(mockMessages.length).toBe(1);
  expect(mockMessages[0]).toStrictEqual(['success', 'Observation sent : 1']);
});

it('Registers Errors', async () => {
  mockSendObservation = (...args) => {
    mockCalls.push(args);
    return {
      error: 'something went wrong',
    };
  };
  const container = shallow(
    <SendObservationModal
      sensor={{ id: 42, name: 'toto' }}
      datePickerClass="datePickerClass"
      inInputClass="inInputClass"
      outInputClass="outInputClass"
      modalClass="modalClass"
    />,
  );
  expect(container.render().text()).toBe('Name toto TimestampInOut');
  await container.find('.datePickerClass').prop('onChange')(
    new Date(1000000000000),
  );
  await container.find('.inInputClass').prop('onChange')(42);
  await container.find('.outInputClass').prop('onChange')(5);
  await container.dive().find('#handleOk').prop('onClick')();
  expect(mockCalls.length).toBe(1);
  expect(mockCalls[0]).toStrictEqual([
    { in: 42, name: 'toto', out: 5, ts: '2001-09-09T01:46:40.000Z' },
  ]);
  expect(mockMessages.length).toBe(1);
  expect(mockMessages[0]).toStrictEqual([
    'error',
    'Observation sent : {"error":"something went wrong"}',
  ]);
});
