import React from 'react';
import { shallow } from 'enzyme';
import VerifiedMetrics from '@/components/metrics/verified-metrics';

jest.mock('antd', () => {
  return {
    __esModule: true,
    ...jest.requireActual('antd'),
    Col: ({ children }) => <>{children}</>,
    Statistic: ({ value }) => <>|{value}|</>,
  };
});

it('renders with null', () => {
  const container = shallow(<VerifiedMetrics />);
  expect(container.render().text()).toContain(
    '|NaN||NaN||||||1970-01-01 00:00:00.0||NaN||NaN||0|',
  );
});

it('renders with values', () => {
  const date = 'Mon Nov 08 2021 18:48:36 GMT+0100';
  const mockParsed = new Date(date);
  const mockDate = new Date(1266424490000);
  jest.spyOn(global, 'Date').mockImplementation((arg) => {
    if (arg === 'Mon Nov 08 2021 18:48:36 GMT+0100') {
      return mockParsed;
    }
    return mockDate;
  });
  const container = shallow(
    <VerifiedMetrics
      lastObservation={'Mon Nov 08 2021 18:48:36 GMT+0100'}
      incoming={10}
      outgoing={100}
      latency={1000}
    />,
  );
  const expectedText =
    '|-90||55||10||100||2010-02-17 16:34:50.0||';
  expect(container.childAt(1).render().text()).toContain(expectedText);
});
