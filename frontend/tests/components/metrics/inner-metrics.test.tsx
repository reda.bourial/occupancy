import React from 'react';
import { shallow } from 'enzyme';
import InnerMetrics from '@/components/metrics/inner-metrics';

jest.mock('antd', () => {
  return {
    __esModule: true,
    ...jest.requireActual('antd'),
    Col: ({ children }) => <>{children}</>,
    Statistic: ({ value }) => <>|{value}|</>,
  };
});

it('renders with null', () => {
  const container = shallow(<InnerMetrics incoming={null} outgoing={null} />);
  expect(container.render().text()).toBe('|0||0|||||');
});

it('shows kpis', () => {
  const container = shallow(<InnerMetrics incoming={2} outgoing={42} />);
  expect(container.render().text()).toBe('|-40||22||2||42|');
});
