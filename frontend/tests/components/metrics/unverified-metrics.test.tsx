import React from 'react';
import { shallow } from 'enzyme';
import UnverifiedMetrics from '@/components/metrics/unverified-metrics';

jest.mock('antd', () => {
  return {
    __esModule: true,
    ...jest.requireActual('antd'),
    Col: ({ children }) => <>{children}</>,
    Statistic: ({ value }) => <>|{value}|</>,
  };
});

it('renders with null', () => {
  const container = shallow(<UnverifiedMetrics />);
  expect(container.render().text()).toContain('|NaN||NaN|||||');
});

it('renders with values', () => {
  const container = shallow(<UnverifiedMetrics outgoing={42} incoming={50} />);
  expect(container.render().text()).toContain('|8||46||50||42|');
});
