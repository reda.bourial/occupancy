import React from 'react';
import { shallow } from 'enzyme';
import Metrics from '@/components/metrics/metrics';

jest.mock('antd', () => {
  return {
    __esModule: true,
    ...jest.requireActual('antd'),
    Col: ({ children }) => <>{children}</>,
    Statistic: ({ value }) => <>|{value}|</>,
  };
});

it('renders with null', () => {
  const container = shallow(
    <Metrics
      counts={{
        out_unverified: null,
        in_unverified: null,
        state_verified: null,
      }}
    />,
  );
  expect(container.childAt(0).name()).toBe('UnverifiedMetrics');
  expect(container.childAt(0).props()).toStrictEqual({
    outgoing: 0,
    incoming: 0,
  });
  expect(container.childAt(1).name()).toBe('VerifiedMetrics');
  expect(container.childAt(1).props()).toStrictEqual({
    lastObservation: 0,
    incoming: 0,
    outgoing: 0,
    latency: 0,
  });
});

it('renders with values', () => {
  const container = shallow(
    <Metrics
      counts={{
        out_unverified: 42,
        in_unverified: 7,
        state_verified: ['2012-01-14 00:00:02', 47, 47, 5],
      }}
    />,
  );
  expect(container.childAt(0).name()).toBe('UnverifiedMetrics');
  expect(container.childAt(0).props()).toStrictEqual({
    outgoing: 42,
    incoming: 7,
  });
  expect(container.childAt(1).name()).toBe('VerifiedMetrics');
  expect(container.childAt(1).props()).toStrictEqual({
    lastObservation: '2012-01-14 00:00:02',
    incoming: 47,
    outgoing: 47,
    latency: 5,
  });
});
