import React from 'react';
import * as ReactRedux from 'react-redux';
import { shallow } from 'enzyme';
import AddSensorModal from '@/components/sensor-modal';

jest.mock('@/components/utils/modal', () => {
  return {
    __esModule: true,
    default: jest.fn((props) => {
      return (
        <div>
          {props.children}
          <button id="handleOk" onClick={props.handleOk} />
        </div>
      );
    }),
  };
});

jest.mock('antd', () => {
  return {
    __esModule: true,
    ...jest.requireActual('antd'),
    message: {
      error: jest.fn(async (...args) => {
        return mockMessage('error', ...args);
      }),
      success: jest.fn(async (...args) => {
        return mockMessage('success', ...args);
      }),
      info: jest.fn(async (...args) => {
        return mockMessage('info', ...args);
      }),
    },
  };
});

let mockDispatch;
let mockDispatches;
let mockMessage;
let mockMessages;

beforeEach(() => {
  mockDispatches = [];
  mockMessages = [];
  mockDispatch = (...args) => mockDispatches.push(args);
  mockMessage = (...args) => mockMessages.push(args);
  jest.spyOn(ReactRedux, 'useDispatch').mockImplementation(() => mockDispatch);
});

it('Creates sensor', async () => {
  mockDispatch = (...args) => {
    mockDispatches.push(args);
    return new Promise((r) => r({ id: 42, name: 'toto' }));
  };
  const container = shallow(
    <AddSensorModal
      sensor={{ id: 42, name: 'toto' }}
      modalClass="modalClass"
      nameInputClass="nameInputClass"
    />,
  );
  expect(container.render().text()).toBe('Name');
  await container.find('.nameInputClass').prop('onChange')({
    target: { value: 'test' },
  });
  await container.dive().find('#handleOk').prop('onClick')();
  expect(mockDispatches.length).toBe(2);
  expect(mockDispatches).toStrictEqual([
    [
      {
        payload: {
          name: 'test',
        },
        type: 'sensor/create',
      },
    ],
    [
      {
        type: 'app/refresh',
      },
    ],
  ]);
  expect(mockMessages.length).toBe(1);
  expect(mockMessages).toStrictEqual([['info', 'creating sensor ...']]);
});

it('ignores empty name', async () => {
  const container = shallow(
    <AddSensorModal
      sensor={{ id: 42, name: 'toto' }}
      modalClass="modalClass"
      nameInputClass="nameInputClass"
    />,
  );
  expect(container.render().text()).toBe('Name');
  await container.find('.nameInputClass').prop('onChange')({
    target: { value: '' },
  });
  await container.dive().find('#handleOk').prop('onClick')();
  expect(mockDispatches.length).toBe(0);
  expect(mockMessages.length).toBe(1);
  expect(mockMessages).toStrictEqual([['info', 'empty name ignored']]);
});

it('Registers Errors', async () => {
  mockDispatch = (...args) => {
    mockDispatches.push(args);
    return new Promise((r) => r({ error: 'something' }));
  };
  const container = shallow(
    <AddSensorModal
      sensor={{ id: 42, name: 'toto' }}
      modalClass="modalClass"
      nameInputClass="nameInputClass"
    />,
  );
  expect(container.render().text()).toBe('Name');
  await container.find('.nameInputClass').prop('onChange')({
    target: { value: 'test' },
  });
  await container.dive().find('#handleOk').prop('onClick')();
  expect(mockDispatches.length).toBe(1);
  expect(mockDispatches).toStrictEqual([
    [
      {
        payload: {
          name: 'test',
        },
        type: 'sensor/create',
      },
    ],
  ]);
  expect(mockMessages.length).toBe(2);
  expect(mockMessages).toStrictEqual([
    ['info', 'creating sensor ...'],
    ['error', 'something went wrong maybe you should change the name.'],
  ]);
});
