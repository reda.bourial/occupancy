import React from 'react';
import * as ReactRedux from 'react-redux';
import { shallow } from 'enzyme';
import SensorCard from '@/components/sensor-card';

let mockDispatch;
let mockDispatches;

beforeEach(() => {
  mockDispatches = [];
  mockDispatch = (...args) => mockDispatches.push(args);
  jest.spyOn(ReactRedux, 'useSelector').mockImplementation((fn) =>
    fn({
      app: {
        sensors: {
          42: {
            id: 42,
            name: 's42',
            counts: {
              state_verified: '[1,2,3,5]',
            },
          },
          50: {
            id: 50,
            name: 's50',
          },
        },
      },
    }),
  );
  jest.spyOn(ReactRedux, 'useDispatch').mockImplementation(() => mockDispatch);
});

it('Gets Sensor with no counts', async () => {
  const container = shallow(<SensorCard sensorId={50} />);
  const expectedSensor = {
    counts: {
      state_verified: [null, null, null, null],
    },
    id: 50,
    name: 's50',
  };
  expect(container.length).toBe(1);
  expect(container.childAt(0).props().actions.length).toBe(2);
  expect(container.childAt(0).props().actions[0].props.sensor).toStrictEqual(
    expectedSensor,
  );
  expect(container.childAt(0).props().actions[1].props.sensor).toStrictEqual(
    expectedSensor,
  );
});

it('Gets Sensor with counts', async () => {
  const container = shallow(<SensorCard sensorId={42} />);
  const expectedSensor = {
    counts: {
      state_verified: [1, 2, 3, 5],
    },
    id: 42,
    name: 's42',
  };
  expect(container.length).toBe(1);
  expect(container.childAt(0).props().actions.length).toBe(2);
  expect(container.childAt(0).props().actions[0].props.sensor).toStrictEqual(
    expectedSensor,
  );
  expect(container.childAt(0).props().actions[1].props.sensor).toStrictEqual(
    expectedSensor,
  );
});

it('Dispatches visibility change', async () => {
  const container = shallow(<SensorCard sensorId={42} />);
  expect(container.length).toBe(1);
  expect(container.props().partialVisibility).toBe(true);
  container.props().onChange(false);
  expect(mockDispatches).toStrictEqual([
    [
      {
        type: 'app/handleVisibilityChange',
        payload: { id: 42, visibility: false },
      },
    ],
  ]);
});
