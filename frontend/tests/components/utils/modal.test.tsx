import React from 'react';
import { shallow } from 'enzyme';
import Modal from '@/components/utils/modal';

jest.mock('antd', () => {
  return {
    __esModule: true,
    ...jest.requireActual('antd'),
    Modal: (...args) => <div {...args} />,
  };
});

let handledOk;
let button;

beforeEach(() => {
  handledOk = false;
  button = <button className="show-modal-button">test</button>;
});

it('renders', () => {
  const container = shallow(
    <Modal
      modalTitle="modal-title"
      modalClass={'modal-class'}
      handleOk={() => {
        handledOk = true;
      }}
      button={button}
      children={'toto'}
    />,
  );
  expect(container.text()).toBe('test');
  expect(handledOk).toBe(false);
  expect(container.find('.modal-class').length).toBe(0);
});

it('appears', () => {
  const container = shallow(
    <Modal
      modalTitle="modal-title"
      modalClass={'modal-class'}
      handleOk={() => {
        handledOk = true;
      }}
      button={button}
      children={'toto'}
    />,
  );
  container.find('b').simulate('click');
  expect(container.find('.modal-class').prop('visible')).toBe(true);
});

it('Okays', () => {
  const container = shallow(
    <Modal
      modalTitle="modal-title"
      modalClass={'modal-class'}
      handleOk={() => {
        handledOk = true;
      }}
      button={button}
      children={'toto'}
    />,
  );
  container.find('b').simulate('click');
  expect(container.find('.modal-class').prop('visible')).toBe(true);
  container.find('.modal-class').prop('onOk')();
  expect(container.find('.modal-class').length).toBe(0);
  expect(handledOk).toBe(true);
});
