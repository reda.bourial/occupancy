import React from 'react';
import { shallow } from 'enzyme';
import SensorList from '@/components/sensor-list';

jest.mock('react-redux', () => {
  return {
    __esModule: true,
    ...jest.requireActual('react'),
    useSelector: (fn) =>
      fn({
        app: {
          sensors: {
            '42': {
              id: 42,
            },
            '50': {
              id: 50,
            },
          },
        },
      }),
  };
});

it('lists sensors', async () => {
  const container = shallow(<SensorList />);
  expect(container.length).toEqual(2);
  expect(container.get(0).props.sensorId).toEqual(50);
  expect(container.get(1).props.sensorId).toEqual(42);
});
