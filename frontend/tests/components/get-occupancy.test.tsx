import React from 'react';
import { shallow } from 'enzyme';
import GetOccupancyModal from '@/components/get-occupancy';

let dispatches;
let mockServiceCalls;

jest.mock('@/components/utils/modal', () => {
  return {
    __esModule: true,
    default: jest.fn(({ children }) => <>{children}</>),
  };
});

jest.mock('antd', () => ({
  ...jest.requireActual('antd'),
  DatePicker: jest.fn((props) => {
    const escapedProps = Object.entries(props)
      .filter(([k, v]) => k != 'showTime')
      .reduce((acc, [k, v]) => {
        acc[k] = v;
        return acc;
      }, {});
    return <div {...escapedProps}> datepicker</div>;
  }),
}));

jest.mock('@/services/observation', () => {
  return {
    __esModule: true,
    getObservation: jest.fn(async (...args) => {
      mockServiceCalls.push(args);
      return {
        type: ' getObservationRet ',
        args: JSON.stringify(args),
      };
    }),
  };
});

beforeEach(() => {
  dispatches = [];
  mockServiceCalls = [];
});

it('gets occupancy', async () => {
  const container = shallow(
    <GetOccupancyModal
      sensor={{ id: 42, name: 'toto' }}
      datePickerClass="datepickerclass"
    />,
  );
  expect(container.render().text()).toBe('Date datepicker');
  await container.find('.datepickerclass').prop('onChange')(
    new Date(1000000000000),
  );
  expect(container.render().text()).toBe(
    'Date datepickertype getObservationRet args["toto","2001-09-09T01:46:40.000Z"]',
  );
});
