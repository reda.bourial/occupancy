import React from 'react';
import * as ReactRedux from 'react-redux';
import { shallow } from 'enzyme';
import WaitForInit from '@/layout/wait-for-connection';

let dispatches;

beforeEach(() => {
  dispatches = [];
  jest
    .spyOn(ReactRedux, 'useDispatch')
    .mockImplementationOnce(() => (e) => dispatches.push(e));
  jest.spyOn(React, 'useEffect').mockImplementation((f) => f());
});

it('shows app', async () => {
  jest
    .spyOn(ReactRedux, 'useSelector')
    .mockImplementationOnce((fn) => fn({ app: { loaded: false } }));
  const container = shallow(<WaitForInit children={<>Hello World !</>} />);
  expect(container.text()).toBe('Loading please wait ...');
  expect(dispatches.length).toBe(1);
  expect(dispatches[0]['type']).toBe('app/init');
});

it('initializes app', async () => {
  jest
    .spyOn(ReactRedux, 'useSelector')
    .mockImplementationOnce((fn) => fn({ app: { loaded: true } }));
  const container = shallow(<WaitForInit children={<>Hello World !</>} />);
  expect(container.text()).toBe('Hello World !');
});
