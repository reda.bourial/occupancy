import React from 'react';
import { shallow } from 'enzyme';
import PaddedBody from '@/layout/padded-body';

it('pads', async () => {
  const container = shallow(<PaddedBody children={<>Hello World !</>} />);
  expect(container.render().text()).toBe('Hello World !');
});
