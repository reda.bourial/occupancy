import logging

from celery import shared_task
from celery_singleton import Singleton

from sensor.models import Sensor

@shared_task(base=Singleton, unique_on=['sensor_id', ])
def tick(sensor_id):
    sensor = Sensor.objects.get(id=sensor_id)
    sensor.synchronize_redis_with_mysql()


@shared_task(base=Singleton,unique_on=['run',])
def tick_all_sensors(run=''):
    logging.info(run)
    for row in Sensor.objects.values_list('id').all():
        identifier = row[0]
        tick.delay(identifier)
