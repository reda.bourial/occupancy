from django.urls import path
from .views import observation_webhook, SensorViewSet, occupancy_view

urlpatterns = [
    path('webhook/', observation_webhook, name='observation-webhook'),
    path('sensor/', SensorViewSet.as_view({
        'get': 'list',
        'post': 'create',
    })),
    path('sensor/<int:pk>', SensorViewSet.as_view({
        'get': 'retrieve',
        'patch': 'update',
    })),
    path('occupancy/', occupancy_view, name="occupancy"),
]
