from collections import deque

from django.db import models, connection
from django.dispatch import receiver


import orjson as json
from django_redis import get_redis_connection

from .basemodel import BaseModel
from .tick import Tick

from .observation import get_create_table_queries, ObservationArchived
from .observation import ObservationUnverified, ObservationVerified


def get_redis():
    return get_redis_connection("default")


def get_value_as_str(conn, key):
    value = conn.get(key)
    if value:
        return value.decode('utf-8')
    return ''


def redis_get(keys):
    with get_redis() as r:
        return {
            json_key: get_value_as_str(r, redis_key)
            for redis_key, json_key
            in keys
        }


def redis_incr(kv):
    with get_redis() as r:
        for k, v in kv:
            if v > 0:
                r.incrby(k, v)
            elif v < 0:
                r.decrby(k, abs(v))


def redis_set(k, v):
    with get_redis() as r:
        r.set(k, v)


class Sensor(BaseModel):
    name = models.CharField(max_length=64)
    count_types = ('unverified',)
    count_attrs = ('in', 'out', 'occupancy')

    class Meta:
        unique_together = [('name',)]

    @property
    def lock_key(self):
        return f"sensor_lock_key_{self.id}"

    def get_key(self, attr, count_type):
        return (
            f"sensor_{attr}_{count_type}_{self.id}",
            f"{attr}_{count_type}",
        )

    @property
    def verified_state_key(self):
        return self.get_key('state', 'verified')

    @property
    def keys(self):
        keys = deque()
        for attr in Sensor.count_attrs:
            for typ in Sensor.count_types:
                keys.append(self.get_key(attr, typ))
        keys.append(self.verified_state_key)
        return keys

    def get_counts(self):
        return redis_get(self.keys)

    def increment_counts(self, incoming, outgoing):
        return redis_incr([
            [self.get_key('in', 'unverified')[0], incoming],
            [self.get_key('out', 'unverified')[0], outgoing],
            [self.get_key('occupancy', 'unverified')[0], incoming-outgoing],
        ])

    @property
    def unverified_table_name(self):
        return ObservationUnverified.get_table_name(self)

    @property
    def verified_table_name(self):
        return ObservationVerified.get_table_name(self)

    @property
    def archived_table_name(self):
        return ObservationArchived.get_table_name(self)

    @property
    def sql_context(self):
        return {
            'unverified_table': self.unverified_table_name,
            'verified_table': self.verified_table_name,
            'archived_table': self.archived_table_name,
            'starting_point': f"starting_point_{self.id}",
            'ending_point': f"ending_point_{self.id}",
            'starting_incoming': f"starting_incoming_{self.id}",
            'starting_outgoing': f"starting_outgoing_{self.id}",
            'nb_event': f"nb_event_{self.id}",
        }

    def queue_observation(self, date, incoming, outgoing):
        sql_context = self.sql_context
        sql_context['values'] = f"""
                    VALUES
                            (NOW(6),
                             NOW(6),
                             {incoming},
                             {outgoing},
                             "{date}");
        """
        sql_context['columns'] = """
                    (`created_at`,
                     `updated_at`,
                     `incoming`,
                     `outgoing`,
                      `ts`)
        """
        sql_query_templates = [
            "BEGIN;"
            """
                    INSERT DELAYED INTO {archived_table}
                    {columns}
                    {values};
            """,
            """
                    INSERT INTO {unverified_table}
                    {columns}
                    {values};
            """,
            "COMMIT;"
        ]
        sql_queries = [
            t.format(**sql_context)
            for t
            in sql_query_templates
        ]
        with connection.cursor() as cursor:
            for query in sql_queries:
                cursor.execute(query)

    def process_observation(self, ts, incoming, outgoing):
        self.increment_counts(incoming, outgoing)
        self.queue_observation(ts, incoming, outgoing)

    def get_count_queries(self):
        sql_context = self.sql_context
        pre_check_queries = [
            "BEGIN;",
            "LOCK TABLES {verified_table} WRITE ,{unverified_table} WRITE;",
            """
                    INSERT INTO {verified_table}
                    (
                        `created_at`,
                        `updated_at`,
                        `deleted_at`,
                        `incoming`,
                        `outgoing`,
                        `ts`
                    )
                    SELECT `created_at`,
                           `updated_at`,
                           `deleted_at`,
                           `incoming`,
                           `outgoing`,
                           `ts`
                    FROM   {unverified_table}
                    ON DUPLICATE KEY UPDATE `nb_repetitions`=ifnull(`nb_repetitions`,0)+1;
                """,
            """
                    SET @{starting_point} = (SELECT MIN(`ts`) from {unverified_table});
                    SET @{ending_point} = (SELECT MAX(`ts`) from {unverified_table});
                    SET @{starting_incoming} = ifnull(
                        (SELECT cumulated_incoming
                         FROM  {verified_table}
                         WHERE ts<@{starting_point}
                         ORDER BY ts DESC LIMIT 1)
                    ,0);
                    SET @{starting_outgoing} = ifnull(
                        (SELECT cumulated_outgoing
                         FROM {verified_table}
                         WHERE ts<@{starting_point}
                         ORDER BY ts DESC LIMIT 1),0);
                    SET @{nb_event} = (SELECT COUNT(*) from {unverified_table});
                """,
            "TRUNCATE  {unverified_table};",
            "UNLOCK TABLES ;"]
        post_check_queries = [
            """
                    UPDATE `{verified_table}` as v,   (
                            SELECT   id,
                                     (
                                        sum(ifnull(incoming,0))
                                        OVER(
                                            ORDER BY ts ASC rows
                                            BETWEEN UNBOUNDED PRECEDING AND CURRENT row
                                            )
                                        +@{starting_incoming})  AS `cumulated_incoming`,
                                     (
                                        sum(ifnull(outgoing,0))
                                        OVER(
                                           ORDER BY ts ASC rows
                                           BETWEEN UNBOUNDED PRECEDING
                                           AND CURRENT row)
                                        +@{starting_outgoing}) AS `cumulated_outgoing`,
                                     ts
                            FROM     `{verified_table}`
                            WHERE    ts>=@{starting_point}
                    ) as temp
                    SET
                        v.cumulated_outgoing = temp.cumulated_outgoing,
                        v.cumulated_incoming = temp.cumulated_incoming,
                        v.updated_at=NOW(6),
                        v.computed_at=NOW(6)
                    WHERE v.id=temp.id ;
                """,
            "COMMIT;",
            """
            SELECT
                @{nb_event},
                @{starting_point},
                @{ending_point},
                @{starting_incoming},
                @{starting_outgoing};
            """,
        ]
        return (
            [q.format(**sql_context) for q in pre_check_queries],
            # pylint: disable=C0301
            "SELECT @{nb_event},@{starting_point},@{ending_point}, @{starting_incoming},@{starting_outgoing};".format(
                **sql_context),
            [q.format(**sql_context) for q in post_check_queries]
        )

    def count(self):
        with connection.cursor() as cursor:
            pre_check, check, post_check = self.get_count_queries()
            for query in pre_check:
                cursor.execute(query)
            cursor.execute(check)
            check_value = cursor.fetchone()
            Tick.objects.create(sensor_id=self.id, value=check_value)
            if check_value[0]:
                for query in post_check:
                    cursor.execute(query)
        return check_value

    def has_queued_observations(self):
        with connection.cursor() as cursor:
            cursor.execute(
                # pylint: disable=C0209
                "SELECT COUNT(*) from {unverified_table};".format(**self.sql_context))
            return cursor.fetchone()[0]

    @property
    def verified_state(self):
        # pylint: disable=C0209
        query = """
                                SELECT
                                    ts,
                                    cumulated_incoming,
                                    cumulated_outgoing,
                                    TIMESTAMPDIFF(MICROSECOND, created_at, computed_at) as processing_delay_in_ns
                                FROM {verified_table}
                                WHERE ts < NOW(6)
                                ORDER BY ts DESC
                                LIMIT 1;
             """.format(**self.sql_context)
        with connection.cursor() as cursor:
            cursor.execute(query)
            return cursor.fetchone()

    def synchronize_redis_with_mysql(self):
        with get_redis() as r:
            with r.lock(self.lock_key):
                if self.has_queued_observations():
                    self.count()
                    state = json.dumps(self.verified_state, default=str)
                    redis_set(self.verified_state_key[0], state)

    def get_occupancy(self, date):
        # pylint: disable=C0301
        condition = f"""
                    ORDER BY ABS(ts-CAST({((f"'{date}'") if date else 'NOW(6)')} AS DATETIME)) ASC
        """
        query = f"""
                    SELECT  id,
                            ts,
                            cumulated_incoming-cumulated_outgoing as occupancy,
                            TIMESTAMPDIFF(MICROSECOND, created_at, computed_at) as processing_delay_in_ns,
                            computed_at,
                            created_at,
                            incoming,
                            outgoing,
                            cumulated_incoming,
                            cumulated_outgoing,
                            nb_repetitions
                    FROM   `{self.verified_table_name}`
                    {condition}
                    LIMIT 1;
                """
        with connection.cursor() as cursor:
            cursor.execute(query)
            data = cursor.fetchall()
        if data:
            column_names = [c[0] for c in cursor.description]
            assert len(data[0]) == len(column_names)
            resp = {
                key: data[0][idx]
                for idx, key in enumerate(column_names)
            }
            return resp
        return None


@receiver(models.signals.post_save, sender=Sensor)
# pylint: disable=W0613
def create_tables(sender, instance, **kwargs):
    with connection.cursor() as cursor:
        for query in get_create_table_queries(instance):
            cursor.execute(query)
