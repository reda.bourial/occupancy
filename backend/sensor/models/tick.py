from django.db import models
from .basemodel import BaseModel

class Tick(BaseModel):
    # avoid race conditions "Table 'sensor_sensor' was not locked with LOCK TABLES"
    sensor_id = models.BigIntegerField()
    value = models.JSONField()
    