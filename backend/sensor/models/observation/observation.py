from django.db import models
from sensor.models.basemodel import BaseModel


class Observation(BaseModel):
    # Sharding at sensor level
    # no need for this foreign key
    # sensor = models.ForeignKey(
    #                on_delete=models.deletion.CASCADE, to='sensor.Sensor')
    incoming = models.IntegerField()
    outgoing = models.IntegerField()
    ts = models.DateTimeField(db_index=True)

    class Meta:
        abstract = True

    @staticmethod
    def get_table_name(sensor):
        raise NotImplementedError()

    @staticmethod
    def get_create_table_queries(sensor):
        raise NotImplementedError()

    @staticmethod
    def pad_id(identifier):
        return str(identifier).rjust(4, '0')
