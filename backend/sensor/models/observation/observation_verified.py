from django.db import models
from .observation import Observation


class ObservationVerified(Observation):
    computed_at = models.DateTimeField(null=True)
    cumulated_incoming = models.IntegerField(default=0, null=True)
    cumulated_outgoing = models.IntegerField(default=0, null=True)
    nb_repetitions = models.IntegerField(default=0, null=True)

    class Meta:
        unique_together = [['ts']]

    @staticmethod
    def get_table_name(sensor):
        return f"sensor_observation_verified_{Observation.pad_id(sensor.id)}"

    @staticmethod
    def get_create_table_queries(sensor):
        table_name = ObservationVerified.get_table_name(sensor)
        return [
            f"""
                CREATE TABLE IF NOT EXISTS {table_name}
                LIKE sensor_observationverified;
            """
        ]
