from .observation_unverified import ObservationUnverified
from .observation_verified import ObservationVerified
from .observation_archived import ObservationArchived

observation_classes = [
    ObservationUnverified,
    ObservationVerified,
    ObservationArchived,
]


def get_create_table_queries(sensor):
    buffer = []
    for observation_class in observation_classes:
        queries = observation_class.get_create_table_queries(sensor)
        buffer.extend(queries)
    return buffer
