from .observation import Observation


class ObservationUnverified(Observation):

    @staticmethod
    def get_table_name(sensor):
        return f"sensor_observation_unverified_{Observation.pad_id(sensor.id)}"

    @staticmethod
    def get_create_table_queries(sensor):
        table_name = ObservationUnverified.get_table_name(sensor)
        return [
            (
                f"""
                CREATE TABLE IF NOT EXISTS {table_name}
                LIKE sensor_observationunverified;
                """
            ),
            (
                f"""
            ALTER TABLE {table_name} ENGINE = MEMORY;
            """
            ),
        ]
