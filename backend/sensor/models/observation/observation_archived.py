from django.db import models
from .observation import Observation


class ObservationArchived(Observation):
    incoming = models.IntegerField()
    outgoing = models.IntegerField()
    ts = models.DateTimeField()

    @staticmethod
    def get_table_name(sensor):
        return f"sensor_observation_archived_{ Observation.pad_id(sensor.id)}"

    @staticmethod
    def get_create_table_queries(sensor):
        table_name = ObservationArchived.get_table_name(sensor)
        return [
            (
                f"""
                CREATE TABLE IF NOT EXISTS {table_name}
                LIKE sensor_observationarchived;
                """
            ),
            (
                f"""
            ALTER TABLE {table_name} ENGINE = MyISAM;
            """
            ),
        ]
