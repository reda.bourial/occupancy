import datetime

from sensor.tests.base import Base

from sensor.models.observation.observation import Observation
from django.db import connection
from freezegun.api import FakeDatetime

class ObservationTest(Base):

    def setUp(self):
        super().setUp()
        self.sensor = self.create_sensor()

    def test_not_implemented_exceptions(self):
        self.assertRaises(NotImplementedError,lambda : Observation.get_table_name(self.sensor))
        self.assertRaises(NotImplementedError,lambda : Observation.get_create_table_queries(self.sensor))
        