import datetime
import json
from sensor.tests.base import Base
from django.db import connection
from freezegun.api import FakeDatetime


class SensorTest(Base):

    def setUp(self):
        super().setUp()
        self.sensor = self.create_sensor()

    def test_tables_get_created(self):
        with connection.cursor() as cursor:
            ret = cursor.execute("SHOW FULL TABLES;")
            tables = [
                r[0]
                for r
                in cursor.fetchall()
            ]
            sensor_tables = [
                self.sensor.unverified_table_name,
                self.sensor.verified_table_name,
                self.sensor.archived_table_name,
            ]
            self.assertGreater(sensor_tables, tables)
            cursor.close()

    def test_get_counts(self):
        counts = self.sensor.get_counts()
        self.assertEqual(counts, {
            'state_verified': '',
            'in_unverified': '',
            'out_unverified': '',
            'occupancy_unverified': '',
        })

    def test_increment_counts(self):
        self.sensor.increment_counts(5, 42)
        counts = self.sensor.get_counts()
        self.assertEqual(counts, {
            'state_verified': '',
            'in_unverified': '5',
            'out_unverified': '42',
            'occupancy_unverified': '-37',
        })
        self.sensor.increment_counts(42, 5)
        counts = self.sensor.get_counts()
        self.assertEqual(counts, {
            'state_verified': '',
            'in_unverified': '47',
            'out_unverified': '47',
            'occupancy_unverified': '0',
        })

    def test_queue_observation(self):
        self.sensor.queue_observation(self.now+self.one_second, 5, 42)
        self.sensor.queue_observation(self.now+2*self.one_second, 42, 5)
        with connection.cursor() as cursor:
            _ = cursor.execute("SELECT incoming,outgoing,ts FROM %s;" %
                               self.sensor.unverified_table_name)
            rows = cursor.fetchall()
            self.assertEqual(
                rows,
                (
                    (5, 42, FakeDatetime(2012, 1, 14, 0, 0, 1)),
                    (42, 5, FakeDatetime(2012, 1, 14, 0, 0, 2)),
                )
            )

            _ = cursor.execute("SELECT incoming,outgoing,ts FROM %s;" %
                               self.sensor.archived_table_name)
            rows = cursor.fetchall()
            self.assertEqual(
                rows,
                ()
            )

    def test_count(self):
        self.sensor.queue_observation(self.now+self.one_second, 5, 42)
        self.sensor.queue_observation(self.now+2*self.one_second, 42, 5)
        ret = self.sensor.count()
        self.assertEqual(ret,
                         (
                             2,
                             '2012-01-14 00:00:01.000000',
                             '2012-01-14 00:00:02.000000',
                             0,
                             0,
                         ))
        with connection.cursor() as cursor:
            _ = cursor.execute("""
                                SELECT 
                                ts,cumulated_incoming,cumulated_outgoing
                                FROM %s;
             """ % self.sensor.verified_table_name)
            rows = cursor.fetchall()
            self.assertEqual(
                rows,
                (
                    (FakeDatetime(2012, 1, 14, 0, 0, 1), 5, 42),
                    (FakeDatetime(2012, 1, 14, 0, 0, 2), 47, 47),
                )
            )

    def test_count_duplicate_events(self):
        self.sensor.queue_observation(self.now+self.one_second, 5, 42)
        self.sensor.queue_observation(self.now+2*self.one_second, 42, 5)
        self.sensor.queue_observation(self.now+2*self.one_second, 42, 5)
        ret = self.sensor.count()
        self.assertEqual(ret,
                         (
                             3,
                             '2012-01-14 00:00:01.000000',
                             '2012-01-14 00:00:02.000000',
                             0,
                             0,
                         ))
        with connection.cursor() as cursor:
            _ = cursor.execute("""
                                SELECT 
                                ts,cumulated_incoming,cumulated_outgoing
                                FROM %s;
             """ % self.sensor.verified_table_name)
            rows = cursor.fetchall()
            self.assertListEqual(
                rows,
                (
                    (FakeDatetime(2012, 1, 14, 0, 0, 1), 5, 42),
                    (FakeDatetime(2012, 1, 14, 0, 0, 2), 47, 47),
                )
            )

    def test_count_duplicate_events(self):
        self.sensor.queue_observation(self.now+self.one_second, 5, 42)
        self.sensor.queue_observation(self.now+2*self.one_second, 42, 5)
        _ = self.sensor.count()
        self.assertListEqual(
            list(self.sensor.verified_state[:-1]),
            [FakeDatetime(2012, 1, 14, 0, 0, 2), 47, 47],
        )

    def test_synchronize_redis(self):
        self.sensor.queue_observation(self.now+self.one_second, 5, 42)
        self.sensor.queue_observation(self.now+2*self.one_second, 42, 5)
        counts = self.sensor.get_counts()
        self.assertEqual(counts, {
            'in_unverified': '',
            'out_unverified': '',
            'occupancy_unverified': '',
            'state_verified': '',
        })
        self.sensor.synchronize_redis_with_mysql()
        counts = self.sensor.get_counts()

        self.assertListEqual(
            json.loads(counts['state_verified'])[:-1],
            ["2012-01-14 00:00:02", 47, 47]
        )

    def test_get_occupancy(self):
        self.sensor.queue_observation(self.now+self.one_second, 5, 42)
        self.sensor.queue_observation(self.now+2*self.one_second, 42, 5)
        _ = self.sensor.count()
        occupancy = self.sensor.get_occupancy('2012-01-14 00:00:01')
        expected = {
            "id": 1,
            "ts": FakeDatetime(2012, 1, 14, 0, 0, 1),
            "occupancy": -37,
            "incoming": 5,
            "outgoing": 42,
            "cumulated_incoming": 5,
            "cumulated_outgoing": 42,
            "nb_repetitions": None,
        }
        for k, v in expected.items():
            self.assertEqual(occupancy[k], v)

    def test_get_occupancy_now(self):
        self.sensor.queue_observation(self.now+self.one_second, 5, 42)
        self.sensor.queue_observation(self.now+2*self.one_second, 42, 5)
        _ = self.sensor.count()
        # avoiding now from python
        with connection.cursor() as cursor:
            cursor.execute("""
                                SELECT 
                                CAST(NOW() AS CHAR CHARACTER SET utf8);
             """)
            now = cursor.fetchone()[0]
        occupancy = self.sensor.get_occupancy(now)
        expected = {
            "id": 2,
            "ts": FakeDatetime(2012, 1, 14, 0, 0, 2),
            "occupancy": 0,
            "incoming": 42,
            "outgoing": 5,
            "cumulated_incoming": 47,
            "cumulated_outgoing": 47,
            "nb_repetitions": None,
        }
        for k, v in expected.items():
            self.assertEqual(occupancy[k], v)

    def test_get_occupancy_no_date(self):
        self.sensor.queue_observation(self.now+self.one_second, 5, 42)
        self.sensor.queue_observation(self.now+2*self.one_second, 42, 5)
        _ = self.sensor.count()
        # avoiding now from python
        occupancy = self.sensor.get_occupancy('')
        expected = {
            "id": 2,
            "ts": FakeDatetime(2012, 1, 14, 0, 0, 2),
            "occupancy": 0,
            "incoming": 42,
            "outgoing": 5,
            "cumulated_incoming": 47,
            "cumulated_outgoing": 47,
            "nb_repetitions": None,
        }
        for k, v in expected.items():
            self.assertEqual(occupancy[k], v)

    def test_get_occupancy_too_early(self):
        self.sensor.queue_observation(self.now+self.one_second, 5, 42)
        self.sensor.queue_observation(self.now+2*self.one_second, 42, 5)
        _ = self.sensor.count()
        occupancy = self.sensor.get_occupancy('1970-01-14 00:00:01')
        expected = {
            "id": 1,
            "ts": FakeDatetime(2012, 1, 14, 0, 0, 1),
            "occupancy": -37,
            "incoming": 5,
            "outgoing": 42,
            "cumulated_incoming": 5,
            "cumulated_outgoing": 42,
            "nb_repetitions": None,
        }
        for k, v in expected.items():
            self.assertEqual(occupancy[k], v)
