import sys
import random
import datetime

from django.test import TransactionTestCase, Client
from model_bakery import baker

from freezegun import freeze_time
from sensor.models import Sensor


@freeze_time("2012-01-14")
class Base(TransactionTestCase):

    def setUp(self):
        super().setUp()
        self.sensor = self.create_sensor()
        self.now = datetime.datetime.now()
        self.one_second = datetime.timedelta(seconds=1)
        self.client = Client()


    def get_random_id(self):
        return random.randint(1, sys.maxsize)

    def create_sensor(self, **kwargs):
        if 'id' not in kwargs:
            kwargs['id'] = self.get_random_id()
        return baker.make(Sensor, **kwargs)
