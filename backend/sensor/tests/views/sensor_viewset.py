from sensor.tests.base import Base
from sensor.views import SensorViewSet
from sensor.serializers import SensorSerializer

class SensorViewSetTest(Base):

    def test_serializer(self):
        self.assertEqual(SensorViewSet.serializer_class, SensorSerializer)

    def test_query(self):
        ids = list(SensorViewSet().get_queryset().values('id').all())
        self.assertEqual(ids, [{'id': self.sensor.id}])
