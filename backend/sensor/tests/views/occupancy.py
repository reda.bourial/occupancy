from django.urls import reverse

from sensor.tests.base import Base

class OccupancyTest(Base):

    def setUp(self):
        super().setUp()
        self.occupancy_url = reverse('occupancy')

    def test_get_occupancy_too_early(self):
        self.sensor.count()
        resp = self.client.get(
            self.occupancy_url,
            data={
                'atInstant': '2012-01-14T00:00:00.002Z',
                'sensor': self.sensor.name,
            }
        )
        self.assertEqual(resp.status_code, 404)
        self.assertEqual(resp.json(), {'error': 'nothing found'})

    def test_get_occupancy(self):
        self.sensor.process_observation(self.now+self.one_second, 5, 42)
        self.sensor.process_observation(self.now+2*self.one_second, 42, 5)
        self.sensor.count()
        resp = self.client.get(
            self.occupancy_url,
            data={
                'atInstant': '2012-01-14T00:00:02.002Z',
                'sensor': self.sensor.name,
            }
        )
        self.assertEqual(resp.status_code, 200)
        expected = {
            'id': 2,
            'ts': '2012-01-14T00:00:02',
            'occupancy': 0,
            'incoming': 42,
            'outgoing': 5,
            'cumulated_incoming': 47,
            'cumulated_outgoing': 47,
            'nb_repetitions': None,
        }
        content = resp.json()
        for k, v in expected.items():
            self.assertEqual(content[k], v)
