from django.urls import reverse

from sensor.tests.base import Base

class ObservationWebhookTest(Base):

    def setUp(self):
        super().setUp()
        self.webhook_url = reverse('observation-webhook')

    def test_send(self):
        resp = self.client.post(self.webhook_url, {
            'name': self.sensor.name,
            'ts': '2011-10-30T12:46:40.840',
            'in': 42,
            'out': 17
        }, content_type="application/json")
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(resp.json(), {'queued': True})
        counts = self.sensor.get_counts()
        self.assertEqual(counts, {
            'state_verified': '',
            'in_unverified': '42',
            'out_unverified': '17',
            'occupancy_unverified': '25',
        })

    def test_send_sql_insertion(self):
        resp = self.client.post(self.webhook_url, {
            'name': self.sensor.name,
            'ts': '--drop tables *;',
            'in': 42,
            'out': 17
        }, content_type="application/json")
        self.assertEqual(resp.status_code, 420)

        resp = self.client.post(self.webhook_url, {
            'name': self.sensor.name,
            'ts': '2011-10-30T12:46:40.840',
            'in': '--drop tables *;',
            'out': 17
        }, content_type="application/json")
        self.assertEqual(resp.status_code, 420)

        resp = self.client.post(self.webhook_url, {
            'name': self.sensor.name,
            'ts': '2011-10-30T12:46:40.840',
            'in': 42,
            'out': '--drop tables *;',
        }, content_type="application/json")
        self.assertEqual(resp.status_code, 420)
