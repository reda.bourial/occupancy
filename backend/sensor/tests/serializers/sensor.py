import datetime

from sensor.tests.base import Base
from sensor.serializers import SensorSerializer
from django.db import connection
from freezegun.api import FakeDatetime


class SensorSerializerTest(Base):

    def setUp(self):
        super().setUp()

    def test_empty(self):
        data = SensorSerializer(self.sensor).data
        self.assertEqual(data, {
            'id': self.sensor.id,
            'counts': {
                'in_unverified': '',
                'out_unverified': '',
                'occupancy_unverified': '',
                'state_verified': '',
            },
            'created_at': '2012-01-14T00:00:00Z',
            'updated_at': '2012-01-14T00:00:00Z',
            'deleted_at': None,
            'name': self.sensor.name,
        })

    def test_full(self):
        self.sensor.process_observation(self.now+self.one_second, 5, 42)
        self.sensor.process_observation(self.now+2*self.one_second, 42, 5)
        _ = self.sensor.count()
        data = SensorSerializer(self.sensor).data
        self.assertEqual(data, {
            'id': self.sensor.id,
            'counts': {
                'in_unverified': '47',
                'out_unverified': '47',
                'occupancy_unverified': '0',
                'state_verified': '',
            },
            'created_at': '2012-01-14T00:00:00Z',
            'updated_at': '2012-01-14T00:00:00Z',
            'deleted_at': None,
            'name': self.sensor.name,
        })

