from rest_framework import serializers

from sensor.models import Sensor

class SensorSerializer(serializers.ModelSerializer):
    counts = serializers.SerializerMethodField()

    class Meta:
        model = Sensor
        fields = '__all__'

    def get_counts(self, sensor):
        return sensor.get_counts()
        