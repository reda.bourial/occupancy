import logging
from random import randrange
import datetime
from django.core.management.base import BaseCommand, CommandError
from functools import cached_property
from sensor.models import Sensor

import dateutil.parser
import time

def create_sensor(name):
    requests.post('http://django:8000/api/sensor' %
                  socket.gethostname(), data={'name': name})


class Command(BaseCommand):
    help = 'Mocks sensors'

    def add_arguments(self, parser):
        parser.add_argument('n', nargs='+', type=int)

    @cached_property
    def sensors_names(self):
        return ["mock_sensor_%d" % i for i in range(1,self.nb_sensors+1)]

    @cached_property
    def sensors(self):
        logging.warn('creating sensors')
        return  [
            Sensor.objects.get_or_create(name=name)[0]
            for name in self.sensors_names
        ]

    def handle(self, *args, **options):
        self.nb_sensors = options['n'][0]
        iteration = 0
        while True:
            iteration += 1
            time.sleep(1/4)
            for sensor in self.sensors:
                incoming=randrange(0, 10)
                outgoing =randrange(0, 10)
                date=str(datetime.datetime.now())
                sensor.increment_counts(incoming, outgoing)
                sensor.queue_observation(date, incoming, outgoing)
            logging.warn('(iteration=%d)' % iteration)
