from django.core.management.base import BaseCommand
from sensor.models import Sensor
from sensor.tasks import tick_all_sensors
import logging
import datetime
import time
from uuid import uuid4

class Command(BaseCommand):
    help = 'Beatscheduler for counting observations'

    def handle(self, *args, **options):
        run_id = str(uuid4()) 
        while True:
            print(datetime.datetime.now(), "ticking")
            try:
                t = tick_all_sensors.delay((run_id,))
                print(datetime.datetime.now(), "sent for ", t)
            except Exception as e:
                print(e)
            time.sleep(0.1)
