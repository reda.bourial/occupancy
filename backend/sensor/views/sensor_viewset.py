from rest_framework import viewsets

from sensor.models import Sensor
from sensor.serializers import SensorSerializer

# pylint: disable=R0901
class SensorViewSet(viewsets.ModelViewSet):
    serializer_class = SensorSerializer

    def get_queryset(self):
        return Sensor.objects.filter(deleted_at=None).all()
