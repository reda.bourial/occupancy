from django.http import JsonResponse

from sensor.models import Sensor
from .common import str_to_sql_date


def occupancy_view(request):
    sensor_name = request.GET['sensor']
    sensor = Sensor.objects.get(name=sensor_name)
    at_instant = request.GET.get('atInstant', '')
    if at_instant:
        at_instant = str_to_sql_date(at_instant)
    data = sensor.get_occupancy(at_instant)
    if data:
        return JsonResponse(data)
    return JsonResponse({
        'error': 'nothing found',
    },status=404)
