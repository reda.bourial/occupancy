import dateutil.parser


def str_to_sql_date(date_str):
    date = dateutil.parser.isoparse(date_str)
    return str(date).split('+', maxsplit=1)[0]
