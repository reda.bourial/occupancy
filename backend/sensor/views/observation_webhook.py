import json

from django.http import JsonResponse

from sensor.models import Sensor
from .common import str_to_sql_date

def parse_observation(request):
    # Sanitization
    body = json.loads(request.body)
    sensor_name = body.get('name', '')
    incoming = int(body.get('in', 0))
    outgoing = int(body.get('out', 0))
    ts = body.get('ts')
    date = str_to_sql_date(ts)
    sensor = Sensor.objects.get(name=sensor_name)
    return sensor, date, incoming, outgoing

def process_request(request):
    sensor, date, incoming, outgoing = parse_observation(request)
    sensor.increment_counts(incoming, outgoing)
    sensor.queue_observation(date, incoming, outgoing)
    return JsonResponse(
        {'queued': True}, safe=True
    )


def observation_webhook(request):
    try:
        return  process_request(request)
    # pylint: disable=W0703
    except Exception as ex:
        return JsonResponse({
            'error': str(ex),
        },status=420)
